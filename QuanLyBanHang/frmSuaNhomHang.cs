﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmSuaNhomHang : Form
    {
        QLBHEntities db = new QLBHEntities();
        NhomHang nh = new NhomHang();

        public frmSuaNhomHang(string id, string ten, string trangthai)
        {
            InitializeComponent();
            LoadTextbox(id, ten, trangthai);
        }

        private void LoadTextbox(string id, string ten, string trangthai)
        {
            txt_MaNhomHang.Text = id;
            txt_TenNhonHang.Text = ten;
            txt_TrangThai.Text = trangthai;
        }

        private void frmSuaNhomHang_Load(object sender, EventArgs e)
        {

        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Luu_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txt_MaNhomHang.Text);
            NhomHang nhomhang = db.NhomHangs.Single(n => n.Id_NhomHang == id);
            nhomhang.TenNhomHang = txt_TenNhonHang.Text;
            nhomhang.TrangThai = 1;
            db.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();
        }
    }
}
