﻿namespace QuanLyBanHang
{
    partial class frmThemDanhSachHangHoa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lueDonVi = new DevExpress.XtraEditors.LookUpEdit();
            this.donViTinhBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dACK_ware2DataSet = new QuanLyBanHang.DACK_ware2DataSet();
            this.luePhanLoai = new DevExpress.XtraEditors.LookUpEdit();
            this.nhomHangBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTonKhoToiThieu = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.txtXuatSu = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTenHang = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaVachNSX = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMaHang = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbGiaBanLe = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbGiaBanSi = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbGiaMua = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbNhaCungCap = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbQuanLy = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.lueTinhChat = new DevExpress.XtraEditors.LookUpEdit();
            this.hangHoaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.hangHoaTableAdapter = new QuanLyBanHang.DACK_ware2DataSetTableAdapters.HangHoaTableAdapter();
            this.lueTenKho = new DevExpress.XtraEditors.LookUpEdit();
            this.khoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.khoTableAdapter = new QuanLyBanHang.DACK_ware2DataSetTableAdapters.KhoTableAdapter();
            this.hangHoaBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.nhomHangTableAdapter = new QuanLyBanHang.DACK_ware2DataSetTableAdapters.NhomHangTableAdapter();
            this.donViTinhTableAdapter = new QuanLyBanHang.DACK_ware2DataSetTableAdapters.DonViTinhTableAdapter();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueDonVi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.donViTinhBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dACK_ware2DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhanLoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nhomHangBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTonKhoToiThieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtXuatSu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaVachNSX.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHang.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaBanLe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaBanSi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaMua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNhaCungCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTinhChat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hangHoaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTenKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.khoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hangHoaBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Loại hàng hóa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(268, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Kho mặc định";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lueDonVi);
            this.groupBox1.Controls.Add(this.luePhanLoai);
            this.groupBox1.Controls.Add(this.textEdit1);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtTonKhoToiThieu);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtXuatSu);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtTenHang);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtMaVachNSX);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtMaHang);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(10, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(554, 219);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin chung";
            // 
            // lueDonVi
            // 
            this.lueDonVi.Location = new System.Drawing.Point(93, 133);
            this.lueDonVi.Name = "lueDonVi";
            this.lueDonVi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDonVi.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenDonVi", "Đơn vị"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_DV", "Mã")});
            this.lueDonVi.Properties.DataSource = this.donViTinhBindingSource;
            this.lueDonVi.Properties.DisplayMember = "TenDonVi";
            this.lueDonVi.Properties.ValueMember = "Id_DV";
            this.lueDonVi.Size = new System.Drawing.Size(131, 20);
            this.lueDonVi.TabIndex = 18;
            // 
            // donViTinhBindingSource
            // 
            this.donViTinhBindingSource.DataMember = "DonViTinh";
            this.donViTinhBindingSource.DataSource = this.dACK_ware2DataSet;
            // 
            // dACK_ware2DataSet
            // 
            this.dACK_ware2DataSet.DataSetName = "DACK_ware2DataSet";
            this.dACK_ware2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // luePhanLoai
            // 
            this.luePhanLoai.Location = new System.Drawing.Point(93, 15);
            this.luePhanLoai.Name = "luePhanLoai";
            this.luePhanLoai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePhanLoai.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenNhomHang", "Nhón hàng"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_NhomHang", "Mã")});
            this.luePhanLoai.Properties.DataSource = this.nhomHangBindingSource;
            this.luePhanLoai.Properties.DisplayMember = "TenNhomHang";
            this.luePhanLoai.Properties.ValueMember = "Id_NhomHang";
            this.luePhanLoai.Size = new System.Drawing.Size(440, 20);
            this.luePhanLoai.TabIndex = 11;
            // 
            // nhomHangBindingSource
            // 
            this.nhomHangBindingSource.DataMember = "NhomHang";
            this.nhomHangBindingSource.DataSource = this.dACK_ware2DataSet;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(93, 165);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(131, 20);
            this.textEdit1.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 172);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 16;
            this.label15.Text = "Tồn kho hiện tại";
            // 
            // txtTonKhoToiThieu
            // 
            this.txtTonKhoToiThieu.Location = new System.Drawing.Point(376, 169);
            this.txtTonKhoToiThieu.Name = "txtTonKhoToiThieu";
            this.txtTonKhoToiThieu.Size = new System.Drawing.Size(131, 20);
            this.txtTonKhoToiThieu.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(258, 172);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Tồn kho tối thiểu";
            // 
            // txtXuatSu
            // 
            this.txtXuatSu.Location = new System.Drawing.Point(376, 133);
            this.txtXuatSu.Name = "txtXuatSu";
            this.txtXuatSu.Size = new System.Drawing.Size(131, 20);
            this.txtXuatSu.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(258, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Xuất sứ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Đơn vị";
            // 
            // txtTenHang
            // 
            this.txtTenHang.Location = new System.Drawing.Point(93, 95);
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(414, 20);
            this.txtTenHang.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Tên hàng";
            // 
            // txtMaVachNSX
            // 
            this.txtMaVachNSX.Location = new System.Drawing.Point(356, 52);
            this.txtMaVachNSX.Name = "txtMaVachNSX";
            this.txtMaVachNSX.Size = new System.Drawing.Size(177, 20);
            this.txtMaVachNSX.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(258, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Mã Vạch NSX";
            // 
            // txtMaHang
            // 
            this.txtMaHang.Location = new System.Drawing.Point(93, 56);
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(131, 20);
            this.txtMaHang.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã hàng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Phân loại";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbGiaBanLe);
            this.groupBox2.Controls.Add(this.cbGiaBanSi);
            this.groupBox2.Controls.Add(this.cbGiaMua);
            this.groupBox2.Controls.Add(this.cbNhaCungCap);
            this.groupBox2.Controls.Add(this.cbQuanLy);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(11, 267);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(552, 163);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin giao dịch";
            // 
            // cbGiaBanLe
            // 
            this.cbGiaBanLe.Location = new System.Drawing.Point(375, 62);
            this.cbGiaBanLe.Name = "cbGiaBanLe";
            this.cbGiaBanLe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGiaBanLe.Size = new System.Drawing.Size(158, 20);
            this.cbGiaBanLe.TabIndex = 24;
            // 
            // cbGiaBanSi
            // 
            this.cbGiaBanSi.Location = new System.Drawing.Point(92, 105);
            this.cbGiaBanSi.Name = "cbGiaBanSi";
            this.cbGiaBanSi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGiaBanSi.Size = new System.Drawing.Size(162, 20);
            this.cbGiaBanSi.TabIndex = 23;
            // 
            // cbGiaMua
            // 
            this.cbGiaMua.Location = new System.Drawing.Point(92, 66);
            this.cbGiaMua.Name = "cbGiaMua";
            this.cbGiaMua.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGiaMua.Size = new System.Drawing.Size(162, 20);
            this.cbGiaMua.TabIndex = 22;
            // 
            // cbNhaCungCap
            // 
            this.cbNhaCungCap.Location = new System.Drawing.Point(92, 27);
            this.cbNhaCungCap.Name = "cbNhaCungCap";
            this.cbNhaCungCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNhaCungCap.Size = new System.Drawing.Size(440, 20);
            this.cbNhaCungCap.TabIndex = 9;
            // 
            // cbQuanLy
            // 
            this.cbQuanLy.AutoSize = true;
            this.cbQuanLy.Location = new System.Drawing.Point(375, 109);
            this.cbQuanLy.Name = "cbQuanLy";
            this.cbQuanLy.Size = new System.Drawing.Size(82, 17);
            this.cbQuanLy.TabIndex = 21;
            this.cbQuanLy.Text = "Còn quản lý";
            this.cbQuanLy.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(309, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "Giá bán lẽ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 109);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Giá bán sĩ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Giá mua";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Nhà cung cấp";
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.simpleButton1.Location = new System.Drawing.Point(488, 436);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 6;
            this.simpleButton1.Text = "Đóng";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.ImageUri.Uri = "Save;Size16x16;Colored";
            this.btnLuu.Location = new System.Drawing.Point(386, 436);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(75, 23);
            this.btnLuu.TabIndex = 7;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.ImageUri.Uri = "Paste;Size16x16;Colored";
            this.simpleButton3.Location = new System.Drawing.Point(11, 436);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(120, 23);
            this.simpleButton3.TabIndex = 8;
            this.simpleButton3.Text = "Lịch sử giao dịch";
            // 
            // lueTinhChat
            // 
            this.lueTinhChat.Location = new System.Drawing.Point(103, 21);
            this.lueTinhChat.Name = "lueTinhChat";
            this.lueTinhChat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTinhChat.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TinhChat", "Tên")});
            this.lueTinhChat.Properties.DataSource = this.hangHoaBindingSource;
            this.lueTinhChat.Properties.DisplayMember = "TinhChat";
            this.lueTinhChat.Properties.ValueMember = "TinhChat";
            this.lueTinhChat.Size = new System.Drawing.Size(131, 20);
            this.lueTinhChat.TabIndex = 9;
            // 
            // hangHoaBindingSource
            // 
            this.hangHoaBindingSource.DataMember = "HangHoa";
            this.hangHoaBindingSource.DataSource = this.dACK_ware2DataSet;
            // 
            // hangHoaTableAdapter
            // 
            this.hangHoaTableAdapter.ClearBeforeFill = true;
            // 
            // lueTenKho
            // 
            this.lueTenKho.Location = new System.Drawing.Point(366, 21);
            this.lueTenKho.Name = "lueTenKho";
            this.lueTenKho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTenKho.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenKho", "Tên Kho"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_Kho", "Mã kho")});
            this.lueTenKho.Properties.DataSource = this.khoBindingSource;
            this.lueTenKho.Properties.DisplayMember = "TenKho";
            this.lueTenKho.Properties.ValueMember = "Id_Kho";
            this.lueTenKho.Size = new System.Drawing.Size(177, 20);
            this.lueTenKho.TabIndex = 10;
            // 
            // khoBindingSource
            // 
            this.khoBindingSource.DataMember = "Kho";
            this.khoBindingSource.DataSource = this.dACK_ware2DataSet;
            // 
            // khoTableAdapter
            // 
            this.khoTableAdapter.ClearBeforeFill = true;
            // 
            // hangHoaBindingSource1
            // 
            this.hangHoaBindingSource1.DataMember = "HangHoa";
            this.hangHoaBindingSource1.DataSource = this.dACK_ware2DataSet;
            // 
            // nhomHangTableAdapter
            // 
            this.nhomHangTableAdapter.ClearBeforeFill = true;
            // 
            // donViTinhTableAdapter
            // 
            this.donViTinhTableAdapter.ClearBeforeFill = true;
            // 
            // frmThemDanhSachHangHoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 471);
            this.Controls.Add(this.lueTenKho);
            this.Controls.Add(this.lueTinhChat);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.btnLuu);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmThemDanhSachHangHoa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm hàng hóa, Dịch vụ";
            this.Load += new System.EventHandler(this.frmThemDanhSachHangHoa_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueDonVi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.donViTinhBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dACK_ware2DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePhanLoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nhomHangBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTonKhoToiThieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtXuatSu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaVachNSX.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaHang.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaBanLe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaBanSi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaMua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNhaCungCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTinhChat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hangHoaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTenKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.khoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hangHoaBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.TextEdit txtTonKhoToiThieu;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.TextEdit txtXuatSu;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit txtTenHang;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit txtMaVachNSX;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit txtMaHang;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.ComboBoxEdit cbGiaBanLe;
        private DevExpress.XtraEditors.ComboBoxEdit cbGiaBanSi;
        private DevExpress.XtraEditors.ComboBoxEdit cbGiaMua;
        private DevExpress.XtraEditors.ComboBoxEdit cbNhaCungCap;
        private System.Windows.Forms.CheckBox cbQuanLy;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.LookUpEdit lueTinhChat;
        private DACK_ware2DataSet dACK_ware2DataSet;
        private System.Windows.Forms.BindingSource hangHoaBindingSource;
        private DACK_ware2DataSetTableAdapters.HangHoaTableAdapter hangHoaTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueTenKho;
        private System.Windows.Forms.BindingSource khoBindingSource;
        private DACK_ware2DataSetTableAdapters.KhoTableAdapter khoTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit luePhanLoai;
        private System.Windows.Forms.BindingSource hangHoaBindingSource1;
        private System.Windows.Forms.BindingSource nhomHangBindingSource;
        private DACK_ware2DataSetTableAdapters.NhomHangTableAdapter nhomHangTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueDonVi;
        private System.Windows.Forms.BindingSource donViTinhBindingSource;
        private DACK_ware2DataSetTableAdapters.DonViTinhTableAdapter donViTinhTableAdapter;
    }
}