﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmSuaDanhSachHangHoa : Form
    {
        QLBHEntities dbe = new QLBHEntities();
        public frmSuaDanhSachHangHoa(string loaihanghoa, string kho, string loai, string mahang, string tenhang, string donvi, string giamua, string giabanle, string giabansi, string trangthai)
        {
            InitializeComponent();
            LoadData(loaihanghoa, kho, loai, mahang, tenhang, donvi, giamua, giabanle, giabansi, trangthai);

        }

        private void frmSuaDanhSachHangHoa_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dACK_ware2DataSet.DonViTinh' table. You can move, or remove it, as needed.
            this.donViTinhTableAdapter.Fill(this.dACK_ware2DataSet.DonViTinh);
            // TODO: This line of code loads data into the 'dACK_ware2DataSet.NhomHang' table. You can move, or remove it, as needed.
            this.nhomHangTableAdapter.Fill(this.dACK_ware2DataSet.NhomHang);
            // TODO: This line of code loads data into the 'dACK_ware2DataSet.Kho' table. You can move, or remove it, as needed.
            this.khoTableAdapter.Fill(this.dACK_ware2DataSet.Kho);
            // TODO: This line of code loads data into the 'dACK_ware2DataSet.HangHoa' table. You can move, or remove it, as needed.
            this.hangHoaTableAdapter.Fill(this.dACK_ware2DataSet.HangHoa);

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            int makho =int.Parse(txtMaHang.Text);
            HangHoa hh = dbe.HangHoas.Single(n=> n.id_hanghoa== makho);
            hh.GiaMua = decimal.Parse(txtGiaMua.Text);
            hh.GiaBanLe = decimal.Parse(txtGiaBanLe.Text);
            hh.GiaBanSi = decimal.Parse(txtGiabanSi.Text);
            hh.Tenhang =  txtTenHang.Text ;
            if (cbQuanLy.Checked == true)
            {
                hh.TrangThai = 1;
            }
            dbe.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
        }
        private void LoadData(string loaihanghoa, string kho, string loai, string mahang, string tenhang, string donvi, string giamua, string giabanle, string giabansi, string trangthai)
        {
            int tt = int.Parse(trangthai);
            if (tt == 1)
            {
                cbQuanLy.Checked = true;
            }
            txtGiaBanLe.Text = giabanle;
            txtGiabanSi.Text = giabansi;
            txtGiaMua.Text = giamua;
            txtMaHang.Text = mahang;
            txtTenHang.Text = tenhang;
           
        }
    }
}
