﻿namespace QuanLyBanHang
{
    partial class frmKhoHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grdDanhSachKhoHang = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_Kho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenKho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.btnThemKhoHang = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoaKho = new DevExpress.XtraEditors.SimpleButton();
            this.btnSuaKho = new DevExpress.XtraEditors.SimpleButton();
            this.btnNapLai = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grdDanhSachKhoHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // grdDanhSachKhoHang
            // 
            this.grdDanhSachKhoHang.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grdDanhSachKhoHang.Location = new System.Drawing.Point(0, 58);
            this.grdDanhSachKhoHang.MainView = this.gridView1;
            this.grdDanhSachKhoHang.Name = "grdDanhSachKhoHang";
            this.grdDanhSachKhoHang.Size = new System.Drawing.Size(733, 345);
            this.grdDanhSachKhoHang.TabIndex = 0;
            this.grdDanhSachKhoHang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_Kho,
            this.TenKho,
            this.gridColumn3,
            this.DiaChi,
            this.SDT,
            this.gridColumn6,
            this.GhiChu,
            this.gridColumn8});
            this.gridView1.GridControl = this.grdDanhSachKhoHang;
            this.gridView1.Name = "gridView1";
            // 
            // Id_Kho
            // 
            this.Id_Kho.Caption = "Mã";
            this.Id_Kho.FieldName = "Id_Kho";
            this.Id_Kho.Name = "Id_Kho";
            this.Id_Kho.Visible = true;
            this.Id_Kho.VisibleIndex = 0;
            // 
            // TenKho
            // 
            this.TenKho.Caption = "Tên";
            this.TenKho.FieldName = "TenKho";
            this.TenKho.Name = "TenKho";
            this.TenKho.Visible = true;
            this.TenKho.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Liên hệ";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // DiaChi
            // 
            this.DiaChi.Caption = "Địa chỉ";
            this.DiaChi.FieldName = "DiaChi";
            this.DiaChi.Name = "DiaChi";
            this.DiaChi.Visible = true;
            this.DiaChi.VisibleIndex = 3;
            // 
            // SDT
            // 
            this.SDT.Caption = "Điện thoại";
            this.SDT.FieldName = "SDT";
            this.SDT.Name = "SDT";
            this.SDT.Visible = true;
            this.SDT.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Ký hiệu";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // GhiChu
            // 
            this.GhiChu.Caption = "Ghi chú";
            this.GhiChu.FieldName = "GhiChu";
            this.GhiChu.Name = "GhiChu";
            this.GhiChu.Visible = true;
            this.GhiChu.VisibleIndex = 6;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Còn quản lý";
            this.gridColumn8.FieldName = "TrangThai";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            // 
            // btnThemKhoHang
            // 
            this.btnThemKhoHang.ImageUri.Uri = "SendBehindText;Size16x16;Colored";
            this.btnThemKhoHang.Location = new System.Drawing.Point(18, 22);
            this.btnThemKhoHang.Name = "btnThemKhoHang";
            this.btnThemKhoHang.Size = new System.Drawing.Size(75, 23);
            this.btnThemKhoHang.TabIndex = 1;
            this.btnThemKhoHang.Text = "Thêm";
            this.btnThemKhoHang.Click += new System.EventHandler(this.btnThemKhoHang_Click);
            // 
            // btnXoaKho
            // 
            this.btnXoaKho.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnXoaKho.Location = new System.Drawing.Point(110, 22);
            this.btnXoaKho.Name = "btnXoaKho";
            this.btnXoaKho.Size = new System.Drawing.Size(75, 23);
            this.btnXoaKho.TabIndex = 2;
            this.btnXoaKho.Text = "Xóa";
            this.btnXoaKho.Click += new System.EventHandler(this.btnXoaKho_Click);
            // 
            // btnSuaKho
            // 
            this.btnSuaKho.ImageUri.Uri = "Replace;Size16x16;Colored";
            this.btnSuaKho.Location = new System.Drawing.Point(208, 22);
            this.btnSuaKho.Name = "btnSuaKho";
            this.btnSuaKho.Size = new System.Drawing.Size(75, 23);
            this.btnSuaKho.TabIndex = 3;
            this.btnSuaKho.Text = "Sửa chữa";
            this.btnSuaKho.Click += new System.EventHandler(this.btnSuaKho_Click);
            // 
            // btnNapLai
            // 
            this.btnNapLai.ImageUri.Uri = "Recurrence;Size16x16;Colored";
            this.btnNapLai.Location = new System.Drawing.Point(304, 22);
            this.btnNapLai.Name = "btnNapLai";
            this.btnNapLai.Size = new System.Drawing.Size(75, 23);
            this.btnNapLai.TabIndex = 4;
            this.btnNapLai.Text = "Nạp lại";
            this.btnNapLai.Click += new System.EventHandler(this.btnNapLai_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageUri.Uri = "Close;Size16x16;Colored";
            this.simpleButton1.Location = new System.Drawing.Point(399, 22);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 5;
            this.simpleButton1.Text = "Đóng";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageUri.Uri = "ExportToXLS;Size16x16;Colored";
            this.simpleButton2.Location = new System.Drawing.Point(491, 22);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 6;
            this.simpleButton2.Text = "Xuất";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // frmKhoHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 403);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnNapLai);
            this.Controls.Add(this.btnSuaKho);
            this.Controls.Add(this.btnXoaKho);
            this.Controls.Add(this.btnThemKhoHang);
            this.Controls.Add(this.grdDanhSachKhoHang);
            this.Name = "frmKhoHang";
            this.Text = "Kho hàng";
            this.Load += new System.EventHandler(this.frmKhoHang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdDanhSachKhoHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdDanhSachKhoHang;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn Id_Kho;
        private DevExpress.XtraGrid.Columns.GridColumn TenKho;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn DiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn SDT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn GhiChu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.SimpleButton btnThemKhoHang;
        private DevExpress.XtraEditors.SimpleButton btnXoaKho;
        private DevExpress.XtraEditors.SimpleButton btnSuaKho;
        private DevExpress.XtraEditors.SimpleButton btnNapLai;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}