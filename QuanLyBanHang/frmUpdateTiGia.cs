﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmUpdateTiGia : DevExpress.XtraEditors.XtraForm
    {
        QLBHEntities dbe = new QLBHEntities();
        TyGia tg = new TyGia();
        public frmUpdateTiGia()
        {
            InitializeComponent();
        }
        public frmUpdateTiGia(string ma, string ten, string quydoi, string trangthai)
        {
            InitializeComponent();
            LoadTextBox(ma, ten, quydoi, trangthai);
        }

        private void LoadTextBox(string ma, string ten, string quydoi, string trangthai)
        {
            
                txtMaTiGia.Text = ma;
                txtTenTiGia.Text = ten;
                int tt = int.Parse(trangthai);
                if (tt == 1)
                {
                    chkTrangThaiTiGia.Checked = true;
                }
                txtTiGiaQuyDoi.Text = quydoi;
           
            
        }

        private void btnCapNhatTiGia_Click(object sender, EventArgs e)
        {
            string id = txtMaTiGia.Text;
            TyGia tg = dbe.TyGias.Single(n => n.id_TyGia == id);
            tg.TenTyGia = txtTenTiGia.Text;
            tg.TyGiaQuyDoi = decimal.Parse( txtTiGiaQuyDoi.Text);
            if (chkTrangThaiTiGia.Checked == true)
            { tg.TrangThai = 1; }
            dbe.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();

        }

        private void btnXoaTiGia_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmUpdateTiGia_Load(object sender, EventArgs e)
        {

        }
    }
}
