﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace QuanLyBanHang
{
    public partial class frmInMaVach : Form
    {
        QLBHEntities dbe = new QLBHEntities();
        

        public frmInMaVach()
        {
            var rs = from a in dbe.MaVaches
                     join b in dbe.HangHoas
                     
                     on a.id_Hang equals b.id_hanghoa
                     join c in dbe.DonViTinhs
                     on b.id_DonVi equals c.Id_DV
                     select new
                     {
                         IDHang = a.id_Hang,
                         IDMaVach = a.id_MaVach,
                         NameProduct = b.Tenhang,
                         NameBarcode  = a.TenTiengViet,
                         Price = b.GiaMua,
                         DonVi = c.TenDonVi
                         

                     };
            InitializeComponent();
            gcInMaVach.DataSource = rs.ToList() ;

        }



        private void frmInMaVach_Load(object sender, EventArgs e)
        {

        }

        private void btnXemHangHoa_Click(object sender, EventArgs e)
        {
            string idhang = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "IDHang").ToString();
            string idmv = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "IDMaVach").ToString();
            string namepro = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "NameProduct").ToString();
            string namebar = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "NameBarcode").ToString();
            string pric = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Price").ToString();
            string donvi = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "DonVi").ToString();
            var form = new frmXemMaVachHangHoa(idhang, idmv, namepro, namebar, pric, donvi);
            form.Show();
        }

        private void btnInMaVach_Click(object sender, EventArgs e)
        {
            gcInMaVach.ExportToXls("ExportMaVach.xls");
            MessageBox.Show("Xuất thành công");
        }

        private void btnInMV_Click(object sender, EventArgs e)
        {
            gcInMaVach.ShowPrintPreview();
        }
    }
}
