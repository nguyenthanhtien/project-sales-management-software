﻿using DevExpress.XtraGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmTyGia : Form
    {
        QLBHEntities dbe = new QLBHEntities();
        TyGia tg = new TyGia();
        DataTable dt = new DataTable();
        public frmTyGia()
        {
            InitializeComponent();
            gcTiGia.DataSource = dbe.TyGias.ToList();
         
        }
        private void LoadFormTiGia()
        {
            gcTiGia.DataSource = dbe.TyGias.ToList();
        }

        private void frmTyGia_Load(object sender, EventArgs e)
        {

        }


        private void btnThemTiGia_Click(object sender, EventArgs e)
        {
            
        }

        private void btnXoaTiGia_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_TyGia").ToString();
                string id = ma;
                TyGia tg = dbe.TyGias.Single(n => n.id_TyGia == id);
                dbe.TyGias.Remove(tg);
                dbe.SaveChanges();
                gcTiGia.DataSource = dbe.TyGias.ToList();
            }
            else
            {
                MessageBox.Show("Xóa không thành công !");
            }

        }
        private void gridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {

        }

        private void btnCapNhatTiGia_Click(object sender, EventArgs e)
        {
            string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_TyGia").ToString();
            string ten = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenTyGia").ToString();
            string quydoi = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TyGiaQuyDoi").ToString();
            string trangthai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TrangThai").ToString();
           
            var form = new frmUpdateTiGia(ma, ten, quydoi, trangthai);
            form.Show();
        }

        private void frmTyGia_Load_1(object sender, EventArgs e)
        {

        }


    }
}