﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemTiGia : Form
    {
        QLBHEntities dbe = new QLBHEntities();
        public frmThemTiGia()
        {
            InitializeComponent();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtMaTiGia.Text != "")
            {
                var tg = new TyGia();
                tg.id_TyGia = txtMaTiGia.Text;
                tg.TenTyGia = txtTenTiGia.Text;
                if (chkHoatDong.Checked == true)
                {
                    tg.TrangThai = 1;
                }
                else
                {
                    tg.TrangThai = 0;
                }
                tg.TyGiaQuyDoi = decimal.Parse( txtGiaTriQuyDoi.Text);
                dbe.TyGias.Add(tg);
                dbe.SaveChanges();
                MessageBox.Show("Thêm thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);

            }
            else
            {
                MessageBox.Show("Vui lòng nhập tên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
