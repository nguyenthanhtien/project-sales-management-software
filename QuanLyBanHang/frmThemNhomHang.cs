﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemNhomHang : Form
    {
        QLBHEntities db = new QLBHEntities();
        NhomHang nh = new NhomHang();
        public frmThemNhomHang()
        {
            InitializeComponent();
        }

        private void frmThemNhomHang_Load(object sender, EventArgs e)
        {
           
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            nh.TenNhomHang = txt_TenNhomHang.Text;
            nh.TrangThai = int.Parse(txt_TrangThai.Text);
            db.NhomHangs.Add(nh);
            db.SaveChanges();
            MessageBox.Show("Thêm thành công!");
            this.Close();

        }
    }
}
