﻿using DACK_ware2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
     
    public partial class frmUpdateNCC : Form
    {
        NhaCungCap ncc = new NhaCungCap();
        QLBHEntities dbe = new QLBHEntities();

        public frmUpdateNCC()
        {
            InitializeComponent();
        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            ncc.id_KV = Int32.Parse(txt_IDKhuVuc.Text);
            ncc.TenNCC = txt_TenNCC.Text;
            ncc.SDT = txt_SDT.Text;
            ncc.ChucVu = txt_ChucVu.Text;
            ncc.DiaChi = txt_DiaChi.Text;
            ncc.TrangThai = Int32.Parse(txt_TrangThai.Text);
            dbe.NhaCungCaps.Add(ncc);
            dbe.SaveChanges();
            MessageBox.Show("Thêm thành công !!");
            this.Close();
        }
    }
}
