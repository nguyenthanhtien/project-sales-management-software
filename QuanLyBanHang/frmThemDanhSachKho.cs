﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemDanhSachKho : Form
    {
        QLBHEntities dbe = new QLBHEntities();
        public frmThemDanhSachKho()
        {
            InitializeComponent();
            cbTinhTrang.Checked = true;
            loadComBoBox();
        }

        private void loadComBoBox()
        {
            var cb = from n in dbe.NhanViens select n.TenNV;
            cmbNguoiQuanLy.DataSource = cb.ToList();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (txtTen.Text != "")
            {
                var kho = new Kho();
                kho.SDT = txtDienthoai.Text;
                kho.GhiChu = txtGhiChu.Text;
                if (cbTinhTrang.Checked == true)
                {
                    kho.TrangThai = 1;
                }
                else
                {
                    kho.TrangThai = 0;
                }
                
                kho.TenKho = txtTen.Text;
                kho.DiaChi = txtDiaChi.Text;
                dbe.Khoes.Add(kho);
                dbe.SaveChanges();
                MessageBox.Show("Thêm thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);

            }
            else
            {
                MessageBox.Show("Vui lòng nhập tên!","Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
