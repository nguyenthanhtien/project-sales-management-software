﻿namespace DACK_ware2
{
    partial class frmKhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gc_KhachHang = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_KhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid_KhuVuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TenKhachHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_DiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_SDT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_email = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_Website = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_MaSoThue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_SoTK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TenNganHang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TrangThai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gc_KhachHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gc_KhachHang
            // 
            this.gc_KhachHang.Location = new System.Drawing.Point(12, 35);
            this.gc_KhachHang.MainView = this.gridView1;
            this.gc_KhachHang.Name = "gc_KhachHang";
            this.gc_KhachHang.Size = new System.Drawing.Size(738, 421);
            this.gc_KhachHang.TabIndex = 0;
            this.gc_KhachHang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_KhachHang,
            this.colid_KhuVuc,
            this.col_TenKhachHang,
            this.col_DiaChi,
            this.col_SDT,
            this.col_email,
            this.col_Website,
            this.col_MaSoThue,
            this.col_SoTK,
            this.col_TenNganHang,
            this.col_TrangThai});
            this.gridView1.GridControl = this.gc_KhachHang;
            this.gridView1.Name = "gridView1";
            // 
            // colid_KhachHang
            // 
            this.colid_KhachHang.Caption = "Mã Khách Hàng";
            this.colid_KhachHang.FieldName = "Id_KH";
            this.colid_KhachHang.Name = "colid_KhachHang";
            this.colid_KhachHang.Visible = true;
            this.colid_KhachHang.VisibleIndex = 0;
            // 
            // colid_KhuVuc
            // 
            this.colid_KhuVuc.Caption = "Mã Khu Vực";
            this.colid_KhuVuc.FieldName = "Id_KhuVuc";
            this.colid_KhuVuc.Name = "colid_KhuVuc";
            this.colid_KhuVuc.Visible = true;
            this.colid_KhuVuc.VisibleIndex = 1;
            // 
            // col_TenKhachHang
            // 
            this.col_TenKhachHang.Caption = "Tên Khách Hàng";
            this.col_TenKhachHang.FieldName = "TenKhachHang";
            this.col_TenKhachHang.Name = "col_TenKhachHang";
            this.col_TenKhachHang.Visible = true;
            this.col_TenKhachHang.VisibleIndex = 2;
            // 
            // col_DiaChi
            // 
            this.col_DiaChi.Caption = "Địa Chỉ";
            this.col_DiaChi.FieldName = "DiaChi";
            this.col_DiaChi.Name = "col_DiaChi";
            this.col_DiaChi.Visible = true;
            this.col_DiaChi.VisibleIndex = 3;
            // 
            // col_SDT
            // 
            this.col_SDT.Caption = "Số Điện Thoại";
            this.col_SDT.FieldName = "SĐT";
            this.col_SDT.Name = "col_SDT";
            this.col_SDT.Visible = true;
            this.col_SDT.VisibleIndex = 4;
            // 
            // col_email
            // 
            this.col_email.Caption = "Email";
            this.col_email.FieldName = "Email";
            this.col_email.Name = "col_email";
            this.col_email.Visible = true;
            this.col_email.VisibleIndex = 5;
            // 
            // col_Website
            // 
            this.col_Website.Caption = "Website";
            this.col_Website.FieldName = "Website";
            this.col_Website.Name = "col_Website";
            this.col_Website.Visible = true;
            this.col_Website.VisibleIndex = 6;
            // 
            // col_MaSoThue
            // 
            this.col_MaSoThue.Caption = "Mã Số Thuế";
            this.col_MaSoThue.FieldName = "MaSoThue";
            this.col_MaSoThue.Name = "col_MaSoThue";
            this.col_MaSoThue.Visible = true;
            this.col_MaSoThue.VisibleIndex = 7;
            // 
            // col_SoTK
            // 
            this.col_SoTK.Caption = "Số Tài Khoản";
            this.col_SoTK.FieldName = "SoTK";
            this.col_SoTK.Name = "col_SoTK";
            this.col_SoTK.Visible = true;
            this.col_SoTK.VisibleIndex = 8;
            // 
            // col_TenNganHang
            // 
            this.col_TenNganHang.Caption = "Tên Ngân Hàng";
            this.col_TenNganHang.FieldName = "TenNganHang";
            this.col_TenNganHang.Name = "col_TenNganHang";
            this.col_TenNganHang.Visible = true;
            this.col_TenNganHang.VisibleIndex = 9;
            // 
            // col_TrangThai
            // 
            this.col_TrangThai.Caption = "Trạng Thái";
            this.col_TrangThai.FieldName = "TrangThai";
            this.col_TrangThai.Name = "col_TrangThai";
            this.col_TrangThai.Visible = true;
            this.col_TrangThai.VisibleIndex = 10;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton1.Location = new System.Drawing.Point(21, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "Thêm";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton2.Location = new System.Drawing.Point(138, 6);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 2;
            this.simpleButton2.Text = "Sửa";
            // 
            // simpleButton3
            // 
            this.simpleButton3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton3.Location = new System.Drawing.Point(267, 6);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 3;
            this.simpleButton3.Text = "Xóa";
            // 
            // simpleButton4
            // 
            this.simpleButton4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton4.Location = new System.Drawing.Point(391, 6);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(75, 23);
            this.simpleButton4.TabIndex = 4;
            this.simpleButton4.Text = "Cập Nhật";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton5.Location = new System.Drawing.Point(513, 6);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(75, 23);
            this.simpleButton5.TabIndex = 5;
            this.simpleButton5.Text = "Đóng";
            // 
            // frmKhachHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 468);
            this.Controls.Add(this.simpleButton5);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.gc_KhachHang);
            this.Name = "frmKhachHang";
            this.Text = "Khách hàng";
            this.Load += new System.EventHandler(this.frmKhachHang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gc_KhachHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gc_KhachHang;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colid_KhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn colid_KhuVuc;
        private DevExpress.XtraGrid.Columns.GridColumn col_TenKhachHang;
        private DevExpress.XtraGrid.Columns.GridColumn col_DiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn col_SDT;
        private DevExpress.XtraGrid.Columns.GridColumn col_email;
        private DevExpress.XtraGrid.Columns.GridColumn col_Website;
        private DevExpress.XtraGrid.Columns.GridColumn col_MaSoThue;
        private DevExpress.XtraGrid.Columns.GridColumn col_SoTK;
        private DevExpress.XtraGrid.Columns.GridColumn col_TenNganHang;
        private DevExpress.XtraGrid.Columns.GridColumn col_TrangThai;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
    }
}