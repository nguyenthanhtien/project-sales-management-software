﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmKhoHang : Form
    {
        QLBHEntities dbe = new QLBHEntities();
        public frmKhoHang()
        {
            InitializeComponent();
            LoadDanhSachKhohang();
        }

        private void LoadDanhSachKhohang()
        {
            grdDanhSachKhoHang.DataSource = dbe.Khoes.ToList();
        }

        private void frmKhoHang_Load(object sender, EventArgs e)
        {

        }

        private void btnThemKhoHang_Click(object sender, EventArgs e)
        {
            var form = new frmThemDanhSachKho();
            form.Show();
        }

        private void btnNapLai_Click(object sender, EventArgs e)
        {
            LoadDanhSachKhohang();
        }

        private void btnSuaKho_Click(object sender, EventArgs e)
        {       
            string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_Kho").ToString();
            string ten = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenKho").ToString();
            string diachi = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "DiaChi").ToString();
            string ghichu = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "GhiChu").ToString();
            string sdt = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "SDT").ToString();
            string trangthai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TrangThai").ToString();
            var form = new frmSuaKhoHang(ma, ten, diachi, ghichu, sdt, trangthai);
            form.Show();
        }

        private void btnXoaKho_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string ma = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_Kho").ToString();
                int id = int.Parse(ma);
                Kho kho = dbe.Khoes.Single(n => n.Id_Kho == id);
                dbe.Khoes.Remove(kho);
                dbe.SaveChanges();
                LoadDanhSachKhohang();
            }
           
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = "C:";
            saveFileDialog1.Title = "Save as Excle File";
            saveFileDialog1.FileName = "";
            saveFileDialog1.Filter = "Excle File(2003)|*.xls|Excle File(2007)|*.xlsx";
            if (saveFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                grdDanhSachKhoHang.ExportToXls(saveFileDialog1.FileName);
            }
        }
    }
}
