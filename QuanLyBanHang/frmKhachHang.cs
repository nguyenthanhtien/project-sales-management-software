﻿using QuanLyBanHang;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACK_ware2
{
    public partial class frmKhachHang : Form
    {
        public frmKhachHang()
        {
            InitializeComponent();
        }

        private void frmKhachHang_Load(object sender, EventArgs e)
        {
            QLBHEntities dbe = new QLBHEntities();
            gc_KhachHang.DataSource = dbe.KhachHangs.ToList();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            QLBHEntities dbe = new QLBHEntities();
            gc_KhachHang.DataSource = dbe.KhachHangs.ToList();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var form = new frmUpdateKH();
            form.Show();
        }
    }
}
