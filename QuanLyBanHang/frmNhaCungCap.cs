﻿using QuanLyBanHang;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACK_ware2
{
    public partial class frmNhaCungCap : Form
    {
        public frmNhaCungCap()
        {
            InitializeComponent();
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            var form = new frmUpdateNCC();
            form.Show();
        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            QLBHEntities dbe = new QLBHEntities();
            gc_NCC.DataSource = dbe.NhaCungCaps.ToList();
        }

        private void frmNhaCungCap_Load(object sender, EventArgs e)
        {
            QLBHEntities dbe = new QLBHEntities();
            gc_NCC.DataSource = dbe.NhaCungCaps.ToList();
        }
    }
}
