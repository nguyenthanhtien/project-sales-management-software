﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThemDanhSachHangHoa : Form
    {
        QLBHEntities db = new QLBHEntities();
        public frmThemDanhSachHangHoa()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (lueDonVi.Text != "" && lueTenKho.Text != "" && lueTinhChat.Text != "")
            {
                // MessageBox.Show(lueDonVi.EditValue.ToString() + luePhanLoai.EditValue.ToString() + lueTinhChat.EditValue.ToString());
                HangHoa hh = new HangHoa();
                hh.Tenhang = txtTenHang.Text;
                hh.GiaMua = Decimal.Parse(cbGiaMua.Text);
                hh.GiaBanSi = Decimal.Parse(cbGiaBanSi.Text);
                hh.GiaBanLe = Decimal.Parse(cbGiaBanLe.Text);
                hh.ToiThieu = int.Parse(txtTonKhoToiThieu.Text);
                if (cbQuanLy.Checked == true)
                {
                    hh.TrangThai = 1;
                }
                hh.TinhChat = lueTinhChat.EditValue.ToString();
                hh.id_nhomhang = int.Parse(lueTenKho.EditValue.ToString());
                hh.id_DonVi = int.Parse(lueDonVi.EditValue.ToString());
                hh.id_kho = int.Parse(lueTenKho.EditValue.ToString());
                db.HangHoas.Add(hh);
                db.SaveChanges();
                MessageBox.Show("Thêm thành công.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin", "Thông báo");
            }
        }
        private void LoadComboBox()
        {
            var tinhchat = from n in db.HangHoas select n.TinhChat;
            lueTinhChat.Properties.DataSource = tinhchat;
        }

        private void frmThemDanhSachHangHoa_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dACK_ware2DataSet.DonViTinh' table. You can move, or remove it, as needed.
            this.donViTinhTableAdapter.Fill(this.dACK_ware2DataSet.DonViTinh);
            // TODO: This line of code loads data into the 'dACK_ware2DataSet.NhomHang' table. You can move, or remove it, as needed.
            this.nhomHangTableAdapter.Fill(this.dACK_ware2DataSet.NhomHang);
            // TODO: This line of code loads data into the 'dACK_ware2DataSet.Kho' table. You can move, or remove it, as needed.
            this.khoTableAdapter.Fill(this.dACK_ware2DataSet.Kho);
            // TODO: This line of code loads data into the 'dACK_ware2DataSet.HangHoa' table. You can move, or remove it, as needed.
            this.hangHoaTableAdapter.Fill(this.dACK_ware2DataSet.HangHoa);

        }
    }
}
