//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuanLyBanHang
{
    using System;
    using System.Collections.Generic;
    
    public partial class VaiTro_ChucNang
    {
        public Nullable<int> id_VT_CN { get; set; }
        public int id_VaiTro { get; set; }
        public int id_ChucNang { get; set; }
        public Nullable<bool> Them { get; set; }
        public Nullable<bool> Xoa { get; set; }
        public Nullable<bool> Sua { get; set; }
        public Nullable<bool> CapNhat { get; set; }
    
        public virtual ChucNang ChucNang { get; set; }
        public virtual VaiTro VaiTro { get; set; }
    }
}
