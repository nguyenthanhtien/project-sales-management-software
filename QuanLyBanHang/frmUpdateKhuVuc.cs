﻿using DACK_ware2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmUpdateKhuVuc : Form
    {
        KhuVuc kv = new KhuVuc();
        QLBHEntities dbe = new QLBHEntities();
        public frmUpdateKhuVuc()
        {
            InitializeComponent();
           
        }

        private void frmUpdateKhuVuc_Load(object sender, EventArgs e)
        {
            
        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            kv.TenKhuVuc = txt_TenKV.Text;
            kv.TrangThai = Int32.Parse(txt_tt.Text);
            dbe.KhuVucs.Add(kv);
            dbe.SaveChanges();
            MessageBox.Show("Thêm thành công !!");
            this.Close();
        }
    }
}
