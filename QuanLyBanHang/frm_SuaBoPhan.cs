﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frm_SuaBoPhan : Form
    {
        QLBHEntities db = new QLBHEntities();
        BoPhan bp = new BoPhan();
        public frm_SuaBoPhan(string id, string tenbophan, string ghichu, string trangthai)
        {
            InitializeComponent();
            LoadTextBox(id, tenbophan, ghichu, trangthai);
        }

        private void frm_SuaBoPhan_Load(object sender, EventArgs e)
        {

        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadTextBox(string id, string tenbophan, string ghichu, string trangthai)
        {
            txt_MaBoPhan.Text = id;
            txt_TenBoPhan.Text = tenbophan;
            txt_GhiChu.Text = ghichu;
            txt_TrangThai.Text = trangthai;
        }

        private void btn_Luu_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txt_MaBoPhan.Text);
            BoPhan bophan = db.BoPhans.Single(n => n.id_BoPhan == id);
            bophan.TenBoPhan = txt_TenBoPhan.Text;
            bophan.GhiChu = txt_GhiChu.Text;
            bophan.TrangThai = 1;
            db.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();
        }
    }
}
