﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frm_ThemBoPhan : Form
    {
        QLBHEntities db = new QLBHEntities();
        BoPhan bp = new BoPhan();
        public frm_ThemBoPhan()
        {
            InitializeComponent();
        }

        private void frm_ThemBoPhan_Load(object sender, EventArgs e)
        {
            
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            //bp.id_BoPhan = int.Parse(txt_MaBoPhan.Text);
            bp.TenBoPhan = txt_TenBoPhan.Text;
            bp.GhiChu = txt_GhiChu.Text;
            bp.TrangThai = int.Parse(txt_TrangThai.Text);
            db.BoPhans.Add(bp);
            db.SaveChanges();
            MessageBox.Show("Thêm thành công !!");
            this.Close();
        }
    }
}
