﻿using DACK_ware2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmUpdateKH : Form
    {
        KhachHang kh  = new KhachHang();
        QLBHEntities dbe = new QLBHEntities();
        public frmUpdateKH()
        {
            InitializeComponent();
        }

        private void frmUpdateKH_Load(object sender, EventArgs e)
        {
            
        }

        private void btn__CapNhat_Click(object sender, EventArgs e)
        {
            kh.Id_KhuVuc = Int32.Parse(txt_idkv.Text);
            kh.TenKhachHang = txt_tenkhachhang.Text;
            kh.DiaChi = txt_DiaChi.Text;
            kh.SĐT = txt_SDT.Text;
            kh.Email = txtEmail.Text;
            kh.Website = txt_Website.Text;
            kh.MaSoThue = Int32.Parse(txt_IDThue.Text);
            kh.SoTK = Int32.Parse(txt_sotk.Text);
            kh.TenNganHang = txt_TenNganHang.Text;
            kh.TrangThai = Int32.Parse(txt_TrangThai.Text);
            dbe.KhachHangs.Add(kh);
            dbe.SaveChanges();
            MessageBox.Show("Thêm thành công !!");
            this.Close();
        }
    }
}
