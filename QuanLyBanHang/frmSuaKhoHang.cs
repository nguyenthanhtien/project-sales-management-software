﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmSuaKhoHang : Form
    {
        QLBHEntities dbe = new QLBHEntities();
        public frmSuaKhoHang(string data, string ten, string diachi, string ghichu, string sdt, string trangthai)
        {
            InitializeComponent();
            LoadTextBox( data, ten,  diachi, ghichu,  sdt,  trangthai);
        }

        private void LoadTextBox(string data, string ten, string diachi, string ghichu, string sdt, string trangthai)
        {
            txtMaKho.Text = data;       
            txtTen.Text = ten;
            int tt = int.Parse(trangthai);
            if (tt == 1)
            {
                cbQuanLy.Checked = true;
            }
            txtDiaChi.Text = diachi;
            txtDienThoai.Text = sdt;
            txtGhiChu.Text = ghichu;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txtMaKho.Text);
            Kho kho = dbe.Khoes.Single(n => n.Id_Kho == id);
            kho.GhiChu = txtGhiChu.Text;
            kho.SDT = txtDienThoai.Text;
            kho.TenKho = txtTen.Text;
            if(cbQuanLy.Checked == true)
            { kho.TrangThai = 1; }
            kho.DiaChi = txtDiaChi.Text;
            dbe.SaveChangesAsync();
            MessageBox.Show("Sửa thành công", "Thông báo",MessageBoxButtons.OK ,MessageBoxIcon.None);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmSuaKhoHang_Load(object sender, EventArgs e)
        {

        }
    }
}
