﻿namespace QuanLyBanHang
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongTinHeThong = new DevExpress.XtraBars.BarButtonItem();
            this.btnPhanQuyen = new DevExpress.XtraBars.BarButtonItem();
            this.btnDoiMatKhau = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhatKyHeThong = new DevExpress.XtraBars.BarButtonItem();
            this.btnSaoLuu = new DevExpress.XtraBars.BarButtonItem();
            this.btnPhucHoi = new DevExpress.XtraBars.BarButtonItem();
            this.btnSuaChua = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.btnKhuVuc = new DevExpress.XtraBars.BarButtonItem();
            this.btnKhachHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhaCungCap = new DevExpress.XtraBars.BarButtonItem();
            this.btnKhoHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnDonViTinh = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhomHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnHangHoa = new DevExpress.XtraBars.BarButtonItem();
            this.btnInMaVach = new DevExpress.XtraBars.BarButtonItem();
            this.btnTiGia = new DevExpress.XtraBars.BarButtonItem();
            this.btnBoPhan = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhanVien = new DevExpress.XtraBars.BarButtonItem();
            this.btnMuaHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnBanHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnTonKho = new DevExpress.XtraBars.BarButtonItem();
            this.btnChuyenKho = new DevExpress.XtraBars.BarButtonItem();
            this.btnNhapSoDuDauKy = new DevExpress.XtraBars.BarButtonItem();
            this.btnThuTien = new DevExpress.XtraBars.BarButtonItem();
            this.btnTraTien = new DevExpress.XtraBars.BarButtonItem();
            this.btnBaoCaoKhoHang = new DevExpress.XtraBars.BarButtonItem();
            this.btnChungTu = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.btnHoTroTrucTuyen = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.btnHuongDanSuDung = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem35 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem36 = new DevExpress.XtraBars.BarButtonItem();
            this.btnLienHe = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItem2 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.barButtonItem32 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem33 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem34 = new DevExpress.XtraBars.BarButtonItem();
            this.btnDangKy = new DevExpress.XtraBars.BarButtonItem();
            this.btnThongTinTroGiup = new DevExpress.XtraBars.BarButtonItem();
            this.btnCapNhat = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.barEditItem3 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemColorEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup12 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup13 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.MdiManager = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MdiManager)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.btnClose,
            this.btnThongTinHeThong,
            this.btnPhanQuyen,
            this.btnDoiMatKhau,
            this.btnNhatKyHeThong,
            this.btnSaoLuu,
            this.btnPhucHoi,
            this.btnSuaChua,
            this.barButtonItem9,
            this.btnKhuVuc,
            this.btnKhachHang,
            this.btnNhaCungCap,
            this.btnKhoHang,
            this.btnDonViTinh,
            this.btnNhomHang,
            this.btnHangHoa,
            this.btnInMaVach,
            this.btnTiGia,
            this.btnBoPhan,
            this.btnNhanVien,
            this.btnMuaHang,
            this.btnBanHang,
            this.btnTonKho,
            this.btnChuyenKho,
            this.btnNhapSoDuDauKy,
            this.btnThuTien,
            this.btnTraTien,
            this.btnBaoCaoKhoHang,
            this.btnChungTu,
            this.barEditItem1,
            this.btnHoTroTrucTuyen,
            this.ribbonGalleryBarItem1,
            this.btnHuongDanSuDung,
            this.btnLienHe,
            this.ribbonGalleryBarItem2,
            this.barButtonItem32,
            this.barButtonItem33,
            this.barButtonItem34,
            this.barButtonItem35,
            this.barButtonItem36,
            this.btnDangKy,
            this.btnThongTinTroGiup,
            this.btnCapNhat,
            this.barEditItem2,
            this.barEditItem3});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 11;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2,
            this.ribbonPage3,
            this.ribbonPage4});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit1,
            this.repositoryItemColorEdit1,
            this.repositoryItemColorEdit2});
            this.ribbonControl1.Size = new System.Drawing.Size(911, 147);
            // 
            // btnClose
            // 
            this.btnClose.Caption = "Đóng";
            this.btnClose.Id = 1;
            this.btnClose.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnClose.ImageOptions.LargeImage")));
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // btnThongTinHeThong
            // 
            this.btnThongTinHeThong.Caption = "Thông tin";
            this.btnThongTinHeThong.Id = 2;
            this.btnThongTinHeThong.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnThongTinHeThong.ImageOptions.LargeImage")));
            this.btnThongTinHeThong.Name = "btnThongTinHeThong";
            this.btnThongTinHeThong.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnThongTin_ItemClick);
            // 
            // btnPhanQuyen
            // 
            this.btnPhanQuyen.Caption = "Phân quyền";
            this.btnPhanQuyen.Id = 3;
            this.btnPhanQuyen.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPhanQuyen.ImageOptions.Image")));
            this.btnPhanQuyen.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnPhanQuyen.ImageOptions.LargeImage")));
            this.btnPhanQuyen.Name = "btnPhanQuyen";
            // 
            // btnDoiMatKhau
            // 
            this.btnDoiMatKhau.Caption = "Đổi mật khẩu";
            this.btnDoiMatKhau.Id = 4;
            this.btnDoiMatKhau.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnDoiMatKhau.ImageOptions.LargeImage")));
            this.btnDoiMatKhau.Name = "btnDoiMatKhau";
            this.btnDoiMatKhau.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // btnNhatKyHeThong
            // 
            this.btnNhatKyHeThong.Caption = "Nhật ký hệ thống";
            this.btnNhatKyHeThong.Id = 5;
            this.btnNhatKyHeThong.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnNhatKyHeThong.ImageOptions.SvgImage")));
            this.btnNhatKyHeThong.Name = "btnNhatKyHeThong";
            // 
            // btnSaoLuu
            // 
            this.btnSaoLuu.Caption = "Sao lưu";
            this.btnSaoLuu.Id = 6;
            this.btnSaoLuu.ImageOptions.ImageUri.Uri = "SaveAll";
            this.btnSaoLuu.Name = "btnSaoLuu";
            // 
            // btnPhucHoi
            // 
            this.btnPhucHoi.Caption = "Phục hồi";
            this.btnPhucHoi.Id = 7;
            this.btnPhucHoi.ImageOptions.ImageUri.Uri = "Refresh";
            this.btnPhucHoi.Name = "btnPhucHoi";
            // 
            // btnSuaChua
            // 
            this.btnSuaChua.Caption = "Sửa chữa";
            this.btnSuaChua.Id = 8;
            this.btnSuaChua.ImageOptions.ImageUri.Uri = "Customization";
            this.btnSuaChua.Name = "btnSuaChua";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Nhập danh sách từ Ecxel";
            this.barButtonItem9.Id = 9;
            this.barButtonItem9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.ImageOptions.Image")));
            this.barButtonItem9.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.ImageOptions.LargeImage")));
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // btnKhuVuc
            // 
            this.btnKhuVuc.Caption = "Khu vực";
            this.btnKhuVuc.Id = 10;
            this.btnKhuVuc.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnKhuVuc.ImageOptions.LargeImage")));
            this.btnKhuVuc.Name = "btnKhuVuc";
            this.btnKhuVuc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKhuVuc_ItemClick);
            // 
            // btnKhachHang
            // 
            this.btnKhachHang.Caption = "Khách hàng";
            this.btnKhachHang.Id = 11;
            this.btnKhachHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnKhachHang.ImageOptions.Image")));
            this.btnKhachHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnKhachHang.ImageOptions.LargeImage")));
            this.btnKhachHang.Name = "btnKhachHang";
            this.btnKhachHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKhachHang_ItemClick);
            // 
            // btnNhaCungCap
            // 
            this.btnNhaCungCap.Caption = "Nhà cung cấp";
            this.btnNhaCungCap.Id = 12;
            this.btnNhaCungCap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNhaCungCap.ImageOptions.Image")));
            this.btnNhaCungCap.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnNhaCungCap.ImageOptions.LargeImage")));
            this.btnNhaCungCap.Name = "btnNhaCungCap";
            this.btnNhaCungCap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhaCungCap_ItemClick);
            // 
            // btnKhoHang
            // 
            this.btnKhoHang.Caption = "Kho hàng";
            this.btnKhoHang.Id = 13;
            this.btnKhoHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnKhoHang.ImageOptions.Image")));
            this.btnKhoHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnKhoHang.ImageOptions.LargeImage")));
            this.btnKhoHang.Name = "btnKhoHang";
            this.btnKhoHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnKhoHang_ItemClick);
            // 
            // btnDonViTinh
            // 
            this.btnDonViTinh.Caption = "Đơn vị tính";
            this.btnDonViTinh.Id = 14;
            this.btnDonViTinh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDonViTinh.ImageOptions.Image")));
            this.btnDonViTinh.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnDonViTinh.ImageOptions.LargeImage")));
            this.btnDonViTinh.Name = "btnDonViTinh";
            this.btnDonViTinh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDonViTinh_ItemClick);
            // 
            // btnNhomHang
            // 
            this.btnNhomHang.Caption = "Nhóm hàng";
            this.btnNhomHang.Id = 15;
            this.btnNhomHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNhomHang.ImageOptions.Image")));
            this.btnNhomHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnNhomHang.ImageOptions.LargeImage")));
            this.btnNhomHang.Name = "btnNhomHang";
            this.btnNhomHang.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhomHang_ItemClick);
            // 
            // btnHangHoa
            // 
            this.btnHangHoa.Caption = "Hàng hóa";
            this.btnHangHoa.Id = 16;
            this.btnHangHoa.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnHangHoa.ImageOptions.Image")));
            this.btnHangHoa.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnHangHoa.ImageOptions.LargeImage")));
            this.btnHangHoa.Name = "btnHangHoa";
            this.btnHangHoa.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnHangHoa_ItemClick);
            // 
            // btnInMaVach
            // 
            this.btnInMaVach.Caption = "In mã vạch";
            this.btnInMaVach.Id = 17;
            this.btnInMaVach.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnInMaVach.ImageOptions.Image")));
            this.btnInMaVach.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnInMaVach.ImageOptions.LargeImage")));
            this.btnInMaVach.Name = "btnInMaVach";
            this.btnInMaVach.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnInMaVach_ItemClick);
            // 
            // btnTiGia
            // 
            this.btnTiGia.Caption = "Tỉ giá";
            this.btnTiGia.Id = 18;
            this.btnTiGia.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnTiGia.ImageOptions.LargeImage")));
            this.btnTiGia.Name = "btnTiGia";
            this.btnTiGia.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTiGia_ItemClick);
            // 
            // btnBoPhan
            // 
            this.btnBoPhan.Caption = "Bộ phận";
            this.btnBoPhan.Id = 19;
            this.btnBoPhan.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBoPhan.ImageOptions.Image")));
            this.btnBoPhan.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnBoPhan.ImageOptions.LargeImage")));
            this.btnBoPhan.Name = "btnBoPhan";
            this.btnBoPhan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnBoPhan_ItemClick);
            // 
            // btnNhanVien
            // 
            this.btnNhanVien.Caption = "Nhân viên";
            this.btnNhanVien.Id = 20;
            this.btnNhanVien.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNhanVien.ImageOptions.Image")));
            this.btnNhanVien.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnNhanVien.ImageOptions.LargeImage")));
            this.btnNhanVien.Name = "btnNhanVien";
            this.btnNhanVien.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNhanVien_ItemClick);
            // 
            // btnMuaHang
            // 
            this.btnMuaHang.Caption = "Mua hàng";
            this.btnMuaHang.Id = 21;
            this.btnMuaHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMuaHang.ImageOptions.Image")));
            this.btnMuaHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnMuaHang.ImageOptions.LargeImage")));
            this.btnMuaHang.Name = "btnMuaHang";
            // 
            // btnBanHang
            // 
            this.btnBanHang.Caption = "Bán hàng";
            this.btnBanHang.Id = 22;
            this.btnBanHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBanHang.ImageOptions.Image")));
            this.btnBanHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnBanHang.ImageOptions.LargeImage")));
            this.btnBanHang.Name = "btnBanHang";
            // 
            // btnTonKho
            // 
            this.btnTonKho.Caption = "Tồn kho";
            this.btnTonKho.Id = 23;
            this.btnTonKho.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnTonKho.ImageOptions.Image")));
            this.btnTonKho.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnTonKho.ImageOptions.LargeImage")));
            this.btnTonKho.Name = "btnTonKho";
            // 
            // btnChuyenKho
            // 
            this.btnChuyenKho.Caption = "Chuyển kho";
            this.btnChuyenKho.Id = 24;
            this.btnChuyenKho.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnChuyenKho.ImageOptions.Image")));
            this.btnChuyenKho.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnChuyenKho.ImageOptions.LargeImage")));
            this.btnChuyenKho.Name = "btnChuyenKho";
            // 
            // btnNhapSoDuDauKy
            // 
            this.btnNhapSoDuDauKy.Caption = "Nhập sổ dư đầu kỳ";
            this.btnNhapSoDuDauKy.Id = 25;
            this.btnNhapSoDuDauKy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNhapSoDuDauKy.ImageOptions.Image")));
            this.btnNhapSoDuDauKy.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnNhapSoDuDauKy.ImageOptions.LargeImage")));
            this.btnNhapSoDuDauKy.Name = "btnNhapSoDuDauKy";
            // 
            // btnThuTien
            // 
            this.btnThuTien.Caption = "Thu tiền";
            this.btnThuTien.Id = 26;
            this.btnThuTien.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnThuTien.ImageOptions.Image")));
            this.btnThuTien.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnThuTien.ImageOptions.LargeImage")));
            this.btnThuTien.Name = "btnThuTien";
            // 
            // btnTraTien
            // 
            this.btnTraTien.Caption = "Trả tiền";
            this.btnTraTien.Id = 27;
            this.btnTraTien.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnTraTien.ImageOptions.Image")));
            this.btnTraTien.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnTraTien.ImageOptions.LargeImage")));
            this.btnTraTien.Name = "btnTraTien";
            // 
            // btnBaoCaoKhoHang
            // 
            this.btnBaoCaoKhoHang.Caption = "Báo cáo kho hàng";
            this.btnBaoCaoKhoHang.Id = 28;
            this.btnBaoCaoKhoHang.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBaoCaoKhoHang.ImageOptions.Image")));
            this.btnBaoCaoKhoHang.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnBaoCaoKhoHang.ImageOptions.LargeImage")));
            this.btnBaoCaoKhoHang.Name = "btnBaoCaoKhoHang";
            // 
            // btnChungTu
            // 
            this.btnChungTu.Caption = "Chứng từ";
            this.btnChungTu.Id = 29;
            this.btnChungTu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnChungTu.ImageOptions.Image")));
            this.btnChungTu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnChungTu.ImageOptions.LargeImage")));
            this.btnChungTu.Name = "btnChungTu";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Edit = this.repositoryItemImageEdit1;
            this.barEditItem1.Id = 31;
            this.barEditItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barEditItem1.ImageOptions.Image")));
            this.barEditItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barEditItem1.ImageOptions.LargeImage")));
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // btnHoTroTrucTuyen
            // 
            this.btnHoTroTrucTuyen.Caption = "Hỗ trợ trực tuyến";
            this.btnHoTroTrucTuyen.Id = 32;
            this.btnHoTroTrucTuyen.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnHoTroTrucTuyen.ImageOptions.Image")));
            this.btnHoTroTrucTuyen.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnHoTroTrucTuyen.ImageOptions.LargeImage")));
            this.btnHoTroTrucTuyen.Name = "btnHoTroTrucTuyen";
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "ribbonGalleryBarItem1";
            this.ribbonGalleryBarItem1.Id = 33;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // btnHuongDanSuDung
            // 
            this.btnHuongDanSuDung.ActAsDropDown = true;
            this.btnHuongDanSuDung.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.btnHuongDanSuDung.Caption = "Hướng dẫn sử dụng";
            this.btnHuongDanSuDung.DropDownControl = this.popupMenu1;
            this.btnHuongDanSuDung.Id = 34;
            this.btnHuongDanSuDung.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnHuongDanSuDung.ImageOptions.LargeImage")));
            this.btnHuongDanSuDung.Name = "btnHuongDanSuDung";
            // 
            // popupMenu1
            // 
            this.popupMenu1.ItemLinks.Add(this.barButtonItem35);
            this.popupMenu1.ItemLinks.Add(this.barButtonItem36);
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbonControl1;
            // 
            // barButtonItem35
            // 
            this.barButtonItem35.Caption = "Tài liệu hướng dẫn";
            this.barButtonItem35.Id = 4;
            this.barButtonItem35.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem35.ImageOptions.Image")));
            this.barButtonItem35.Name = "barButtonItem35";
            // 
            // barButtonItem36
            // 
            this.barButtonItem36.Caption = "Video Hướng dẫn";
            this.barButtonItem36.Id = 5;
            this.barButtonItem36.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem36.ImageOptions.Image")));
            this.barButtonItem36.Name = "barButtonItem36";
            // 
            // btnLienHe
            // 
            this.btnLienHe.Caption = "Liên hệ";
            this.btnLienHe.Id = 35;
            this.btnLienHe.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLienHe.ImageOptions.Image")));
            this.btnLienHe.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnLienHe.ImageOptions.LargeImage")));
            this.btnLienHe.Name = "btnLienHe";
            // 
            // ribbonGalleryBarItem2
            // 
            this.ribbonGalleryBarItem2.Caption = "ribbonGalleryBarItem2";
            this.ribbonGalleryBarItem2.Id = 36;
            this.ribbonGalleryBarItem2.Name = "ribbonGalleryBarItem2";
            // 
            // barButtonItem32
            // 
            this.barButtonItem32.Caption = "Hello";
            this.barButtonItem32.Id = 1;
            this.barButtonItem32.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem32.ImageOptions.Image")));
            this.barButtonItem32.Name = "barButtonItem32";
            // 
            // barButtonItem33
            // 
            this.barButtonItem33.Caption = "Tài liệu hướng dẫn";
            this.barButtonItem33.Id = 2;
            this.barButtonItem33.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem33.ImageOptions.Image")));
            this.barButtonItem33.Name = "barButtonItem33";
            // 
            // barButtonItem34
            // 
            this.barButtonItem34.Caption = "Video Hướng dẫn";
            this.barButtonItem34.Id = 3;
            this.barButtonItem34.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem34.ImageOptions.Image")));
            this.barButtonItem34.Name = "barButtonItem34";
            // 
            // btnDangKy
            // 
            this.btnDangKy.Caption = "Đăng ký";
            this.btnDangKy.Id = 6;
            this.btnDangKy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDangKy.ImageOptions.Image")));
            this.btnDangKy.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnDangKy.ImageOptions.LargeImage")));
            this.btnDangKy.Name = "btnDangKy";
            // 
            // btnThongTinTroGiup
            // 
            this.btnThongTinTroGiup.Caption = "Thông tin";
            this.btnThongTinTroGiup.Id = 7;
            this.btnThongTinTroGiup.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnThongTinTroGiup.ImageOptions.LargeImage")));
            this.btnThongTinTroGiup.Name = "btnThongTinTroGiup";
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Caption = "Cập nhật";
            this.btnCapNhat.Id = 8;
            this.btnCapNhat.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCapNhat.ImageOptions.Image")));
            this.btnCapNhat.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCapNhat.ImageOptions.LargeImage")));
            this.btnCapNhat.Name = "btnCapNhat";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Edit = this.repositoryItemColorEdit1;
            this.barEditItem2.Id = 9;
            this.barEditItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barEditItem2.ImageOptions.Image")));
            this.barEditItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barEditItem2.ImageOptions.LargeImage")));
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            // 
            // barEditItem3
            // 
            this.barEditItem3.Caption = "barEditItem3";
            this.barEditItem3.Edit = this.repositoryItemColorEdit2;
            this.barEditItem3.Id = 10;
            this.barEditItem3.Name = "barEditItem3";
            // 
            // repositoryItemColorEdit2
            // 
            this.repositoryItemColorEdit2.AutoHeight = false;
            this.repositoryItemColorEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit2.Name = "repositoryItemColorEdit2";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Hệ thống";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnClose);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnThongTinHeThong);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Hệ thống";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnPhanQuyen);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnDoiMatKhau);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnNhatKyHeThong);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Bảo mật";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnSaoLuu);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnPhucHoi);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnSuaChua);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Dữ liệu";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6,
            this.ribbonPageGroup9});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Danh mục";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem9);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Excel";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.btnKhuVuc);
            this.ribbonPageGroup5.ItemLinks.Add(this.btnKhachHang);
            this.ribbonPageGroup5.ItemLinks.Add(this.btnNhaCungCap);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Đối tác";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.btnKhoHang);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnDonViTinh);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnNhomHang);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnHangHoa);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnInMaVach);
            this.ribbonPageGroup6.ItemLinks.Add(this.btnTiGia);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Kho hàng";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.btnBoPhan);
            this.ribbonPageGroup9.ItemLinks.Add(this.btnNhanVien);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.Text = "Tổ chức";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup7,
            this.ribbonPageGroup10,
            this.ribbonPageGroup11,
            this.ribbonPageGroup12});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Chức năng";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.btnMuaHang);
            this.ribbonPageGroup7.ItemLinks.Add(this.btnBanHang);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "Nhập - Xuất";
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.ItemLinks.Add(this.btnTonKho);
            this.ribbonPageGroup10.ItemLinks.Add(this.btnChuyenKho);
            this.ribbonPageGroup10.ItemLinks.Add(this.btnNhapSoDuDauKy);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            this.ribbonPageGroup10.Text = "Kho hàng";
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.ItemLinks.Add(this.btnThuTien);
            this.ribbonPageGroup11.ItemLinks.Add(this.btnTraTien);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            this.ribbonPageGroup11.Text = "Công nợ";
            // 
            // ribbonPageGroup12
            // 
            this.ribbonPageGroup12.ItemLinks.Add(this.btnBaoCaoKhoHang);
            this.ribbonPageGroup12.ItemLinks.Add(this.btnChungTu);
            this.ribbonPageGroup12.Name = "ribbonPageGroup12";
            this.ribbonPageGroup12.Text = "Báo cáo";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup8,
            this.ribbonPageGroup13});
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "Trợ giúp";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.btnHoTroTrucTuyen);
            this.ribbonPageGroup8.ItemLinks.Add(this.btnHuongDanSuDung);
            this.ribbonPageGroup8.ItemLinks.Add(this.btnLienHe);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.Text = "Trợ giúp";
            // 
            // ribbonPageGroup13
            // 
            this.ribbonPageGroup13.ItemLinks.Add(this.btnDangKy);
            this.ribbonPageGroup13.ItemLinks.Add(this.btnThongTinTroGiup);
            this.ribbonPageGroup13.ItemLinks.Add(this.btnCapNhat);
            this.ribbonPageGroup13.Name = "ribbonPageGroup13";
            this.ribbonPageGroup13.Text = "Thông tin";
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Office 2013";
            // 
            // MdiManager
            // 
            this.MdiManager.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeader;
            this.MdiManager.MdiParent = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 571);
            this.Controls.Add(this.ribbonControl1);
            this.IsMdiContainer = true;
            this.Name = "Form1";
            this.Ribbon = this.ribbonControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PHẦN MỀM QUẢN LÝ BÁN HÀNG";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MdiManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraBars.BarButtonItem btnThongTinHeThong;
        private DevExpress.XtraBars.BarButtonItem btnPhanQuyen;
        private DevExpress.XtraBars.BarButtonItem btnDoiMatKhau;
        private DevExpress.XtraBars.BarButtonItem btnNhatKyHeThong;
        private DevExpress.XtraBars.BarButtonItem btnSaoLuu;
        private DevExpress.XtraBars.BarButtonItem btnPhucHoi;
        private DevExpress.XtraBars.BarButtonItem btnSuaChua;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem btnKhuVuc;
        private DevExpress.XtraBars.BarButtonItem btnKhachHang;
        private DevExpress.XtraBars.BarButtonItem btnNhaCungCap;
        private DevExpress.XtraBars.BarButtonItem btnKhoHang;
        private DevExpress.XtraBars.BarButtonItem btnDonViTinh;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.BarButtonItem btnNhomHang;
        private DevExpress.XtraBars.BarButtonItem btnHangHoa;
        private DevExpress.XtraBars.BarButtonItem btnInMaVach;
        private DevExpress.XtraBars.BarButtonItem btnTiGia;
        private DevExpress.XtraBars.BarButtonItem btnBoPhan;
        private DevExpress.XtraBars.BarButtonItem btnNhanVien;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.BarButtonItem btnMuaHang;
        private DevExpress.XtraBars.BarButtonItem btnBanHang;
        private DevExpress.XtraBars.BarButtonItem btnTonKho;
        private DevExpress.XtraBars.BarButtonItem btnChuyenKho;
        private DevExpress.XtraBars.BarButtonItem btnNhapSoDuDauKy;
        private DevExpress.XtraBars.BarButtonItem btnThuTien;
        private DevExpress.XtraBars.BarButtonItem btnTraTien;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup12;
        private DevExpress.XtraBars.BarButtonItem btnBaoCaoKhoHang;
        private DevExpress.XtraBars.BarButtonItem btnChungTu;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraBars.BarButtonItem btnHoTroTrucTuyen;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.BarButtonItem btnHuongDanSuDung;
        private DevExpress.XtraBars.BarButtonItem btnLienHe;
        private DevExpress.XtraBars.BarButtonItem barButtonItem32;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem2;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem35;
        private DevExpress.XtraBars.BarButtonItem barButtonItem36;
        private DevExpress.XtraBars.BarButtonItem barButtonItem33;
        private DevExpress.XtraBars.BarButtonItem barButtonItem34;
        private DevExpress.XtraBars.BarButtonItem btnDangKy;
        private DevExpress.XtraBars.BarButtonItem btnThongTinTroGiup;
        private DevExpress.XtraBars.BarButtonItem btnCapNhat;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup13;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraBars.BarEditItem barEditItem3;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit2;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager MdiManager;
    }
}

