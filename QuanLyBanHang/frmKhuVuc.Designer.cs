﻿namespace DACK_ware2
{
    partial class frmKhuVuc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gckhuvuc = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_MaKhuVuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TenKhuVuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TrangThai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn_Them = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Sua = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Load = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gckhuvuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gckhuvuc
            // 
            this.gckhuvuc.Location = new System.Drawing.Point(12, 36);
            this.gckhuvuc.MainView = this.gridView1;
            this.gckhuvuc.Name = "gckhuvuc";
            this.gckhuvuc.Size = new System.Drawing.Size(486, 386);
            this.gckhuvuc.TabIndex = 0;
            this.gckhuvuc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_MaKhuVuc,
            this.col_TenKhuVuc,
            this.col_TrangThai});
            this.gridView1.GridControl = this.gckhuvuc;
            this.gridView1.Name = "gridView1";
            // 
            // colid_MaKhuVuc
            // 
            this.colid_MaKhuVuc.Caption = "Mã Khu Vực";
            this.colid_MaKhuVuc.FieldName = "Id_KhuVuc";
            this.colid_MaKhuVuc.Name = "colid_MaKhuVuc";
            this.colid_MaKhuVuc.Visible = true;
            this.colid_MaKhuVuc.VisibleIndex = 0;
            // 
            // col_TenKhuVuc
            // 
            this.col_TenKhuVuc.Caption = "Tên Khu Vực";
            this.col_TenKhuVuc.FieldName = "TenKhuVuc";
            this.col_TenKhuVuc.Name = "col_TenKhuVuc";
            this.col_TenKhuVuc.Visible = true;
            this.col_TenKhuVuc.VisibleIndex = 1;
            // 
            // col_TrangThai
            // 
            this.col_TrangThai.Caption = "Trạng Thái";
            this.col_TrangThai.FieldName = "TrangThai";
            this.col_TrangThai.Name = "col_TrangThai";
            this.col_TrangThai.Visible = true;
            this.col_TrangThai.VisibleIndex = 2;
            // 
            // btn_Them
            // 
            this.btn_Them.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Them.Location = new System.Drawing.Point(12, 7);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(75, 23);
            this.btn_Them.TabIndex = 1;
            this.btn_Them.Text = "Thêm";
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // btn_Xoa
            // 
            this.btn_Xoa.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Xoa.Location = new System.Drawing.Point(195, 7);
            this.btn_Xoa.Name = "btn_Xoa";
            this.btn_Xoa.Size = new System.Drawing.Size(75, 23);
            this.btn_Xoa.TabIndex = 2;
            this.btn_Xoa.Text = "Xóa";
            // 
            // btn_Sua
            // 
            this.btn_Sua.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Sua.Location = new System.Drawing.Point(103, 7);
            this.btn_Sua.Name = "btn_Sua";
            this.btn_Sua.Size = new System.Drawing.Size(75, 23);
            this.btn_Sua.TabIndex = 3;
            this.btn_Sua.Text = "Sửa";
            // 
            // btn_Load
            // 
            this.btn_Load.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Load.Location = new System.Drawing.Point(287, 7);
            this.btn_Load.Name = "btn_Load";
            this.btn_Load.Size = new System.Drawing.Size(75, 23);
            this.btn_Load.TabIndex = 4;
            this.btn_Load.Text = "Cập Nhật";
            this.btn_Load.Click += new System.EventHandler(this.btn_Load_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton2.Location = new System.Drawing.Point(379, 7);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 5;
            this.simpleButton2.Text = "Đóng";
            // 
            // frmKhuVuc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 434);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.btn_Load);
            this.Controls.Add(this.btn_Sua);
            this.Controls.Add(this.btn_Xoa);
            this.Controls.Add(this.btn_Them);
            this.Controls.Add(this.gckhuvuc);
            this.Name = "frmKhuVuc";
            this.Text = "Khu Vực";
            this.Load += new System.EventHandler(this.frmKhuVuc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gckhuvuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gckhuvuc;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btn_Them;
        private DevExpress.XtraEditors.SimpleButton btn_Xoa;
        private DevExpress.XtraEditors.SimpleButton btn_Sua;
        private DevExpress.XtraGrid.Columns.GridColumn colid_MaKhuVuc;
        private DevExpress.XtraGrid.Columns.GridColumn col_TenKhuVuc;
        private DevExpress.XtraGrid.Columns.GridColumn col_TrangThai;
        private DevExpress.XtraEditors.SimpleButton btn_Load;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
    }
}