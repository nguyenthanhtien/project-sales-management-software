﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmBoPhan : Form
    {
        QLBHEntities db = new QLBHEntities();
        public frmBoPhan()
        {
            InitializeComponent();
            LoadDuLieu();
        }

        private void LoadDuLieu()
        {
            gc_BoPhan.DataSource = db.BoPhans.ToList();
        }

        private void frmBoPhan_Load(object sender, EventArgs e)
        {

        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            var form = new frm_ThemBoPhan();
            form.Show();
        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            LoadDuLieu();

        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Sua_Click(object sender, EventArgs e)
        {
            string id = gridView1.GetRowCellValue(gridView1.FocusedRowHandle,"id_BoPhan").ToString();
            string ten = gridView1.GetRowCellValue(gridView1.FocusedRowHandle,"TenBoPhan").ToString();
            string ghichu = gridView1.GetRowCellValue(gridView1.FocusedRowHandle,"GhiChu").ToString();
            string trangthai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle,"TrangThai").ToString();
            var form = new frm_SuaBoPhan(id, ten, ghichu, trangthai);
            form.Show();
        }

        private void btn_Xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string mabophan = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id_BoPhan").ToString();
                int id = int.Parse(mabophan);
                BoPhan bophan = db.BoPhans.Single(n => n.id_BoPhan == id);
                db.BoPhans.Remove(bophan);
                db.SaveChanges();
                LoadDuLieu();
            }
            else
            {
                MessageBox.Show("Không xoá");
            }
        }
    }
}
