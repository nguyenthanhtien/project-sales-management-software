//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuanLyBanHang
{
    using System;
    using System.Collections.Generic;
    
    public partial class ThuTien
    {
        public int id_ChungTuThuTien { get; set; }
        public Nullable<int> id_KhuVuc { get; set; }
        public Nullable<System.DateTime> Ngay { get; set; }
        public Nullable<int> id_KhachHang { get; set; }
        public string TenKH { get; set; }
        public Nullable<decimal> SoTien { get; set; }
        public Nullable<decimal> DaTra { get; set; }
        public Nullable<decimal> ConLai { get; set; }
    
        public virtual KhachHang KhachHang { get; set; }
        public virtual KhuVuc KhuVuc { get; set; }
    }
}
