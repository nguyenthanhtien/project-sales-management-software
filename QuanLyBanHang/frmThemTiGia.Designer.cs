﻿namespace QuanLyBanHang
{
    partial class frmThemTiGia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaTiGia = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenTiGia = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGiaTriQuyDoi = new DevExpress.XtraEditors.TextEdit();
            this.chkHoatDong = new System.Windows.Forms.CheckBox();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTiGia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenTiGia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaTriQuyDoi.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã tỉ giá :";
            // 
            // txtMaTiGia
            // 
            this.txtMaTiGia.Location = new System.Drawing.Point(108, 20);
            this.txtMaTiGia.Name = "txtMaTiGia";
            this.txtMaTiGia.Size = new System.Drawing.Size(132, 20);
            this.txtMaTiGia.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên tỉ giá :";
            // 
            // txtTenTiGia
            // 
            this.txtTenTiGia.Location = new System.Drawing.Point(108, 46);
            this.txtTenTiGia.Name = "txtTenTiGia";
            this.txtTenTiGia.Size = new System.Drawing.Size(132, 20);
            this.txtTenTiGia.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Giá trị quy đổi :";
            // 
            // txtGiaTriQuyDoi
            // 
            this.txtGiaTriQuyDoi.Location = new System.Drawing.Point(108, 72);
            this.txtGiaTriQuyDoi.Name = "txtGiaTriQuyDoi";
            this.txtGiaTriQuyDoi.Size = new System.Drawing.Size(132, 20);
            this.txtGiaTriQuyDoi.TabIndex = 1;
            // 
            // chkHoatDong
            // 
            this.chkHoatDong.AutoSize = true;
            this.chkHoatDong.Location = new System.Drawing.Point(160, 98);
            this.chkHoatDong.Name = "chkHoatDong";
            this.chkHoatDong.Size = new System.Drawing.Size(77, 17);
            this.chkHoatDong.TabIndex = 2;
            this.chkHoatDong.Text = "Hoạt động";
            this.chkHoatDong.UseVisualStyleBackColor = true;
            // 
            // btnThem
            // 
            this.btnThem.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btnThem.Location = new System.Drawing.Point(12, 130);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(93, 30);
            this.btnThem.TabIndex = 3;
            this.btnThem.Text = "Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnDong
            // 
            this.btnDong.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnDong.Location = new System.Drawing.Point(144, 130);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(93, 30);
            this.btnDong.TabIndex = 3;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // frmThemTiGia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 182);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.chkHoatDong);
            this.Controls.Add(this.txtGiaTriQuyDoi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTenTiGia);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtMaTiGia);
            this.Controls.Add(this.label1);
            this.Name = "frmThemTiGia";
            this.Text = "Thêm tỉ giá";
            ((System.ComponentModel.ISupportInitialize)(this.txtMaTiGia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenTiGia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGiaTriQuyDoi.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtMaTiGia;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txtTenTiGia;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit txtGiaTriQuyDoi;
        private System.Windows.Forms.CheckBox chkHoatDong;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraEditors.SimpleButton btnDong;
    }
}