﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmNhomHang : Form
    {
        QLBHEntities db = new QLBHEntities();
        NhomHang nh = new NhomHang();

        public frmNhomHang()
        {
            InitializeComponent();
            LoadDuLieu();
        }

        private void LoadDuLieu()
        {
            gc_NhomHang.DataSource = db.NhomHangs.ToList();
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            var form = new frmThemNhomHang();
            form.Show();
        }

        private void btn_CapNhat_Click(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.Cancel)
            {
                string manhomhang = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_NhomHang").ToString();
                int id = int.Parse(manhomhang);
                NhomHang nhomhang = db.NhomHangs.Single(n => n.Id_NhomHang == id);
                db.NhomHangs.Remove(nhomhang);
                db.SaveChanges();
                LoadDuLieu();
            }
            else
            {
                MessageBox.Show("Không xoá");
            }
        }

        private void btn_Sua_Click(object sender, EventArgs e)
        {
            string id = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id_NhomHang").ToString();
            string ten = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TenNhomHang").ToString();
            string trangthai = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TrangThai").ToString();
            var form = new frmSuaNhomHang(id, ten, trangthai);
            form.Show();
        }

        private void frmNhomHang_Load(object sender, EventArgs e)
        {

        }
    }
}
