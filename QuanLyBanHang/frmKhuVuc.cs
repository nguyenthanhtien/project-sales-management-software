﻿using QuanLyBanHang;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACK_ware2
{
    public partial class frmKhuVuc : Form
    {
        public frmKhuVuc()
        {
            InitializeComponent();
        }

        private void frmKhuVuc_Load(object sender, EventArgs e)
        {
            QLBHEntities dbe = new QLBHEntities();
            gckhuvuc.DataSource = dbe.KhuVucs.ToList();
        }

        private void btn_Load_Click(object sender, EventArgs e)
        {
            QLBHEntities dbe = new QLBHEntities();
            gckhuvuc.DataSource = dbe.KhuVucs.ToList();
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            var form = new frmUpdateKhuVuc();
            form.Show();
        }
    }
}
