﻿namespace QuanLyBanHang
{
    partial class frmBoPhan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gc_BoPhan = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_BoPhan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TenBoPhan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_GhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TinhTrang = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn_Them = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Xoa = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Sua = new DevExpress.XtraEditors.SimpleButton();
            this.btn_CapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Close = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gc_BoPhan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gc_BoPhan
            // 
            this.gc_BoPhan.Location = new System.Drawing.Point(0, 32);
            this.gc_BoPhan.MainView = this.gridView1;
            this.gc_BoPhan.Name = "gc_BoPhan";
            this.gc_BoPhan.Size = new System.Drawing.Size(740, 377);
            this.gc_BoPhan.TabIndex = 0;
            this.gc_BoPhan.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_BoPhan,
            this.col_TenBoPhan,
            this.col_GhiChu,
            this.col_TinhTrang});
            this.gridView1.GridControl = this.gc_BoPhan;
            this.gridView1.Name = "gridView1";
            // 
            // colid_BoPhan
            // 
            this.colid_BoPhan.Caption = "Mã Bộ Phận";
            this.colid_BoPhan.FieldName = "id_BoPhan";
            this.colid_BoPhan.Name = "colid_BoPhan";
            this.colid_BoPhan.Visible = true;
            this.colid_BoPhan.VisibleIndex = 0;
            // 
            // col_TenBoPhan
            // 
            this.col_TenBoPhan.Caption = "Tên Bộ Phận";
            this.col_TenBoPhan.FieldName = "TenBoPhan";
            this.col_TenBoPhan.Name = "col_TenBoPhan";
            this.col_TenBoPhan.Visible = true;
            this.col_TenBoPhan.VisibleIndex = 1;
            // 
            // col_GhiChu
            // 
            this.col_GhiChu.Caption = "Ghi Chú";
            this.col_GhiChu.FieldName = "GhiChu";
            this.col_GhiChu.Name = "col_GhiChu";
            this.col_GhiChu.Visible = true;
            this.col_GhiChu.VisibleIndex = 2;
            // 
            // col_TinhTrang
            // 
            this.col_TinhTrang.Caption = "Tình Trạng";
            this.col_TinhTrang.FieldName = "TinhTrang";
            this.col_TinhTrang.Name = "col_TinhTrang";
            this.col_TinhTrang.Visible = true;
            this.col_TinhTrang.VisibleIndex = 3;
            // 
            // btn_Them
            // 
            this.btn_Them.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Them.ImageOptions.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btn_Them.Location = new System.Drawing.Point(0, 0);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(69, 26);
            this.btn_Them.TabIndex = 2;
            this.btn_Them.Text = "Thêm";
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // btn_Xoa
            // 
            this.btn_Xoa.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Xoa.ImageOptions.Image = global::QuanLyBanHang.Properties.Resources.trash_16x16;
            this.btn_Xoa.ImageOptions.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btn_Xoa.Location = new System.Drawing.Point(84, 0);
            this.btn_Xoa.Name = "btn_Xoa";
            this.btn_Xoa.Size = new System.Drawing.Size(69, 26);
            this.btn_Xoa.TabIndex = 3;
            this.btn_Xoa.Text = "Xoá";
            this.btn_Xoa.Click += new System.EventHandler(this.btn_Xoa_Click);
            // 
            // btn_Sua
            // 
            this.btn_Sua.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Sua.ImageOptions.Image = global::QuanLyBanHang.Properties.Resources.editcomment_16x16;
            this.btn_Sua.ImageOptions.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btn_Sua.Location = new System.Drawing.Point(169, 0);
            this.btn_Sua.Name = "btn_Sua";
            this.btn_Sua.Size = new System.Drawing.Size(69, 26);
            this.btn_Sua.TabIndex = 4;
            this.btn_Sua.Text = "Sửa";
            this.btn_Sua.Click += new System.EventHandler(this.btn_Sua_Click);
            // 
            // btn_CapNhat
            // 
            this.btn_CapNhat.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_CapNhat.ImageOptions.Image = global::QuanLyBanHang.Properties.Resources.convert_16x16;
            this.btn_CapNhat.ImageOptions.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btn_CapNhat.Location = new System.Drawing.Point(256, 0);
            this.btn_CapNhat.Name = "btn_CapNhat";
            this.btn_CapNhat.Size = new System.Drawing.Size(85, 26);
            this.btn_CapNhat.TabIndex = 5;
            this.btn_CapNhat.Text = "Cập Nhật";
            this.btn_CapNhat.Click += new System.EventHandler(this.btn_CapNhat_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Close.ImageOptions.Image = global::QuanLyBanHang.Properties.Resources.close_16x16;
            this.btn_Close.ImageOptions.ImageUri.Uri = "Add;Size16x16;Colored";
            this.btn_Close.Location = new System.Drawing.Point(358, 0);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(69, 26);
            this.btn_Close.TabIndex = 6;
            this.btn_Close.Text = "Đóng";
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // frmBoPhan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 410);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_CapNhat);
            this.Controls.Add(this.btn_Sua);
            this.Controls.Add(this.btn_Xoa);
            this.Controls.Add(this.btn_Them);
            this.Controls.Add(this.gc_BoPhan);
            this.Name = "frmBoPhan";
            this.Text = "Bộ phận";
            this.Load += new System.EventHandler(this.frmBoPhan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gc_BoPhan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gc_BoPhan;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btn_Them;
        private DevExpress.XtraEditors.SimpleButton btn_Xoa;
        private DevExpress.XtraEditors.SimpleButton btn_Sua;
        private DevExpress.XtraEditors.SimpleButton btn_CapNhat;
        private DevExpress.XtraEditors.SimpleButton btn_Close;
        private DevExpress.XtraGrid.Columns.GridColumn colid_BoPhan;
        private DevExpress.XtraGrid.Columns.GridColumn col_TenBoPhan;
        private DevExpress.XtraGrid.Columns.GridColumn col_GhiChu;
        private DevExpress.XtraGrid.Columns.GridColumn col_TinhTrang;
    }
}