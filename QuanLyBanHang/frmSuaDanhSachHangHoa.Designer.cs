﻿namespace QuanLyBanHang
{
    partial class frmSuaDanhSachHangHoa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lueTinhChat = new DevExpress.XtraEditors.LookUpEdit();
            this.lueKho = new DevExpress.XtraEditors.LookUpEdit();
            this.dACK_ware2DataSet = new QuanLyBanHang.DACK_ware2DataSet();
            this.hangHoaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.hangHoaTableAdapter = new QuanLyBanHang.DACK_ware2DataSetTableAdapters.HangHoaTableAdapter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lueNhomHang = new DevExpress.XtraEditors.LookUpEdit();
            this.txtMaHang = new System.Windows.Forms.TextBox();
            this.txtMaVachNSX = new System.Windows.Forms.TextBox();
            this.txtTenHang = new System.Windows.Forms.TextBox();
            this.louDonVi = new DevExpress.XtraEditors.LookUpEdit();
            this.txtXuatSu = new System.Windows.Forms.TextBox();
            this.txtTonKhoToiThieu = new System.Windows.Forms.TextBox();
            this.txtTonKhoHienTai = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.txtGiabanSi = new System.Windows.Forms.TextBox();
            this.txtGiaBanLe = new System.Windows.Forms.TextBox();
            this.cbQuanLy = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.khoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.khoTableAdapter = new QuanLyBanHang.DACK_ware2DataSetTableAdapters.KhoTableAdapter();
            this.nhomHangBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nhomHangTableAdapter = new QuanLyBanHang.DACK_ware2DataSetTableAdapters.NhomHangTableAdapter();
            this.txtGiaMua = new System.Windows.Forms.TextBox();
            this.donViTinhBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.donViTinhTableAdapter = new QuanLyBanHang.DACK_ware2DataSetTableAdapters.DonViTinhTableAdapter();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.lueTinhChat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dACK_ware2DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hangHoaBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueNhomHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.louDonVi.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.khoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nhomHangBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.donViTinhBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lueTinhChat
            // 
            this.lueTinhChat.Location = new System.Drawing.Point(116, 28);
            this.lueTinhChat.Name = "lueTinhChat";
            this.lueTinhChat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTinhChat.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TinhChat", "Tính chất")});
            this.lueTinhChat.Properties.DataSource = this.hangHoaBindingSource;
            this.lueTinhChat.Properties.DisplayMember = "TinhChat";
            this.lueTinhChat.Properties.ValueMember = "TinhChat";
            this.lueTinhChat.Size = new System.Drawing.Size(134, 20);
            this.lueTinhChat.TabIndex = 0;
            // 
            // lueKho
            // 
            this.lueKho.Location = new System.Drawing.Point(349, 28);
            this.lueKho.Name = "lueKho";
            this.lueKho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKho.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenKho", "Tên kho"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_Kho", "Mã kho")});
            this.lueKho.Properties.DataSource = this.khoBindingSource;
            this.lueKho.Properties.DisplayMember = "TenKho";
            this.lueKho.Properties.ValueMember = "Id_Kho";
            this.lueKho.Size = new System.Drawing.Size(141, 20);
            this.lueKho.TabIndex = 1;
            // 
            // dACK_ware2DataSet
            // 
            this.dACK_ware2DataSet.DataSetName = "DACK_ware2DataSet";
            this.dACK_ware2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // hangHoaBindingSource
            // 
            this.hangHoaBindingSource.DataMember = "HangHoa";
            this.hangHoaBindingSource.DataSource = this.dACK_ware2DataSet;
            // 
            // hangHoaTableAdapter
            // 
            this.hangHoaTableAdapter.ClearBeforeFill = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtTonKhoToiThieu);
            this.groupBox1.Controls.Add(this.txtTonKhoHienTai);
            this.groupBox1.Controls.Add(this.txtXuatSu);
            this.groupBox1.Controls.Add(this.louDonVi);
            this.groupBox1.Controls.Add(this.txtTenHang);
            this.groupBox1.Controls.Add(this.txtMaVachNSX);
            this.groupBox1.Controls.Add(this.txtMaHang);
            this.groupBox1.Controls.Add(this.lueNhomHang);
            this.groupBox1.Location = new System.Drawing.Point(13, 51);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(484, 180);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin chung";
            // 
            // lueNhomHang
            // 
            this.lueNhomHang.Location = new System.Drawing.Point(103, 19);
            this.lueNhomHang.Name = "lueNhomHang";
            this.lueNhomHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueNhomHang.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenNhomHang", "Nhóm hàng"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_NhomHang", "Mã")});
            this.lueNhomHang.Properties.DataSource = this.nhomHangBindingSource;
            this.lueNhomHang.Properties.DisplayMember = "TenNhomHang";
            this.lueNhomHang.Properties.ValueMember = "Id_NhomHang";
            this.lueNhomHang.Size = new System.Drawing.Size(374, 20);
            this.lueNhomHang.TabIndex = 3;
            // 
            // txtMaHang
            // 
            this.txtMaHang.Location = new System.Drawing.Point(103, 55);
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(134, 20);
            this.txtMaHang.TabIndex = 4;
            // 
            // txtMaVachNSX
            // 
            this.txtMaVachNSX.Location = new System.Drawing.Point(336, 55);
            this.txtMaVachNSX.Name = "txtMaVachNSX";
            this.txtMaVachNSX.Size = new System.Drawing.Size(141, 20);
            this.txtMaVachNSX.TabIndex = 5;
            // 
            // txtTenHang
            // 
            this.txtTenHang.Location = new System.Drawing.Point(103, 91);
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(374, 20);
            this.txtTenHang.TabIndex = 6;
            // 
            // louDonVi
            // 
            this.louDonVi.Location = new System.Drawing.Point(103, 124);
            this.louDonVi.Name = "louDonVi";
            this.louDonVi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.louDonVi.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenDonVi", "Đơn vị"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_DV", "Mã")});
            this.louDonVi.Properties.DataSource = this.donViTinhBindingSource;
            this.louDonVi.Properties.DisplayMember = "TenDonVi";
            this.louDonVi.Properties.ValueMember = "Id_DV";
            this.louDonVi.Size = new System.Drawing.Size(134, 20);
            this.louDonVi.TabIndex = 3;
            // 
            // txtXuatSu
            // 
            this.txtXuatSu.Location = new System.Drawing.Point(336, 124);
            this.txtXuatSu.Name = "txtXuatSu";
            this.txtXuatSu.Size = new System.Drawing.Size(141, 20);
            this.txtXuatSu.TabIndex = 7;
            // 
            // txtTonKhoToiThieu
            // 
            this.txtTonKhoToiThieu.Location = new System.Drawing.Point(336, 154);
            this.txtTonKhoToiThieu.Name = "txtTonKhoToiThieu";
            this.txtTonKhoToiThieu.Size = new System.Drawing.Size(141, 20);
            this.txtTonKhoToiThieu.TabIndex = 9;
            // 
            // txtTonKhoHienTai
            // 
            this.txtTonKhoHienTai.Location = new System.Drawing.Point(103, 154);
            this.txtTonKhoHienTai.Name = "txtTonKhoHienTai";
            this.txtTonKhoHienTai.Size = new System.Drawing.Size(134, 20);
            this.txtTonKhoHienTai.TabIndex = 8;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtGiaMua);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.cbQuanLy);
            this.groupBox2.Controls.Add(this.lookUpEdit4);
            this.groupBox2.Controls.Add(this.txtGiabanSi);
            this.groupBox2.Controls.Add(this.txtGiaBanLe);
            this.groupBox2.Location = new System.Drawing.Point(12, 239);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(484, 136);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin giao dịch";
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.Location = new System.Drawing.Point(104, 34);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TinhChat", "Tính chất")});
            this.lookUpEdit4.Properties.DataSource = this.hangHoaBindingSource;
            this.lookUpEdit4.Properties.DisplayMember = "TinhChat";
            this.lookUpEdit4.Properties.ValueMember = "TinhChat";
            this.lookUpEdit4.Size = new System.Drawing.Size(374, 20);
            this.lookUpEdit4.TabIndex = 10;
            // 
            // txtGiabanSi
            // 
            this.txtGiabanSi.Location = new System.Drawing.Point(104, 101);
            this.txtGiabanSi.Name = "txtGiabanSi";
            this.txtGiabanSi.Size = new System.Drawing.Size(134, 20);
            this.txtGiabanSi.TabIndex = 12;
            // 
            // txtGiaBanLe
            // 
            this.txtGiaBanLe.Location = new System.Drawing.Point(337, 71);
            this.txtGiaBanLe.Name = "txtGiaBanLe";
            this.txtGiaBanLe.Size = new System.Drawing.Size(141, 20);
            this.txtGiaBanLe.TabIndex = 11;
            // 
            // cbQuanLy
            // 
            this.cbQuanLy.AutoSize = true;
            this.cbQuanLy.Location = new System.Drawing.Point(337, 104);
            this.cbQuanLy.Name = "cbQuanLy";
            this.cbQuanLy.Size = new System.Drawing.Size(82, 17);
            this.cbQuanLy.TabIndex = 13;
            this.cbQuanLy.Text = "Còn quản lý";
            this.cbQuanLy.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Loại hàng hóa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(270, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Kho mặc định";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Phân loại";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(256, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Mã vạch NSX";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Mã hàng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Tên hàng";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Đơn vị";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(284, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Xuất sứ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Tồn kho hiện tại";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(243, 157);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Tồn kho tối thiểu";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 37);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Nhà cung cấp";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 71);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Giá mua";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(273, 74);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "Giá bán lẽ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 105);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 20;
            this.label14.Text = "Giá bán sĩ";
            // 
            // khoBindingSource
            // 
            this.khoBindingSource.DataMember = "Kho";
            this.khoBindingSource.DataSource = this.dACK_ware2DataSet;
            // 
            // khoTableAdapter
            // 
            this.khoTableAdapter.ClearBeforeFill = true;
            // 
            // nhomHangBindingSource
            // 
            this.nhomHangBindingSource.DataMember = "NhomHang";
            this.nhomHangBindingSource.DataSource = this.dACK_ware2DataSet;
            // 
            // nhomHangTableAdapter
            // 
            this.nhomHangTableAdapter.ClearBeforeFill = true;
            // 
            // txtGiaMua
            // 
            this.txtGiaMua.Location = new System.Drawing.Point(104, 75);
            this.txtGiaMua.Name = "txtGiaMua";
            this.txtGiaMua.Size = new System.Drawing.Size(134, 20);
            this.txtGiaMua.TabIndex = 21;
            // 
            // donViTinhBindingSource
            // 
            this.donViTinhBindingSource.DataMember = "DonViTinh";
            this.donViTinhBindingSource.DataSource = this.dACK_ware2DataSet;
            // 
            // donViTinhTableAdapter
            // 
            this.donViTinhTableAdapter.ClearBeforeFill = true;
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageUri.Uri = "Save;Size16x16;Colored";
            this.simpleButton1.Location = new System.Drawing.Point(237, 381);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 6;
            this.simpleButton1.Text = "Lưu";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.simpleButton2.Location = new System.Drawing.Point(349, 381);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 7;
            this.simpleButton2.Text = "Đóng";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // frmSuaDanhSachHangHoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 416);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lueKho);
            this.Controls.Add(this.lueTinhChat);
            this.Name = "frmSuaDanhSachHangHoa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmSuaDanhSachHangHoa";
            this.Load += new System.EventHandler(this.frmSuaDanhSachHangHoa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lueTinhChat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dACK_ware2DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hangHoaBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueNhomHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.louDonVi.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.khoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nhomHangBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.donViTinhBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit lueTinhChat;
        private DevExpress.XtraEditors.LookUpEdit lueKho;
        private DACK_ware2DataSet dACK_ware2DataSet;
        private System.Windows.Forms.BindingSource hangHoaBindingSource;
        private DACK_ware2DataSetTableAdapters.HangHoaTableAdapter hangHoaTableAdapter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTonKhoToiThieu;
        private System.Windows.Forms.TextBox txtTonKhoHienTai;
        private System.Windows.Forms.TextBox txtXuatSu;
        private DevExpress.XtraEditors.LookUpEdit louDonVi;
        private System.Windows.Forms.TextBox txtTenHang;
        private System.Windows.Forms.TextBox txtMaVachNSX;
        private System.Windows.Forms.TextBox txtMaHang;
        private DevExpress.XtraEditors.LookUpEdit lueNhomHang;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox cbQuanLy;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private System.Windows.Forms.TextBox txtGiabanSi;
        private System.Windows.Forms.TextBox txtGiaBanLe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource khoBindingSource;
        private DACK_ware2DataSetTableAdapters.KhoTableAdapter khoTableAdapter;
        private System.Windows.Forms.BindingSource nhomHangBindingSource;
        private DACK_ware2DataSetTableAdapters.NhomHangTableAdapter nhomHangTableAdapter;
        private System.Windows.Forms.TextBox txtGiaMua;
        private System.Windows.Forms.BindingSource donViTinhBindingSource;
        private DACK_ware2DataSetTableAdapters.DonViTinhTableAdapter donViTinhTableAdapter;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
    }
}