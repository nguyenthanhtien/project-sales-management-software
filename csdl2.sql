USE [master]
GO
if DB_ID('DACK_ware2') is not null
    drop database DACK_ware2
go
/****** Object:  Database [DACK_ware]    Script Date: 5/19/2017 3:06:52 AM ******/
CREATE DATABASE [DACK_ware2]
 
GO
USE [DACK_ware2]
GO

CREATE TABLE [dbo].[BanHang](
	[Id_kho] [int] NULL,
	[id_ChungTuban] [int] IDENTITY(1,1) NOT NULL,
	[id_khachhang] [int] NULL,
	[NgayBan] [datetime] NULL CONSTRAINT [DF_BanHang_NgayBan]  DEFAULT (getdate()),
	[SoPhieuVietTay] [int] NULL,
	[SoTien] [money] NULL,
	[ChieuKhau] [int] NULL,
	[Vat] [int] NULL,
	[ThanhToan] [money] NULL,
 CONSTRAINT [PK_BanHang] PRIMARY KEY (id_ChungTuban))
/****** Object:  Table [dbo].[BoPhan]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[BoPhan](
	[id_BoPhan] [int] IDENTITY(1,1) NOT NULL,
	[TenBoPhan] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](50) NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_BoPhan] PRIMARY KEY (id_BoPhan))
/****** Object:  Table [dbo].[ChiTien]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[ChiTien](
	[id_ChungTuChiTien] [int] IDENTITY(1,1) NOT NULL,
	[id_KhuVuc] [int] NULL,
	[Ngay] [datetime] NULL CONSTRAINT [DF_ChiTien_Ngay]  DEFAULT (getdate()),
	[id_NCC] [int] NULL,
	[Ten_NCC] [nvarchar](100) NULL,
	[SoTien] [money] NULL,
	[DaTra] [money] NULL,
	[ConLai] [money] NULL,
 CONSTRAINT [PK_ChiTien] PRIMARY KEY (id_ChungTuChiTien))
/****** Object:  Table [dbo].[ChiTietBanHang]    Script Date: 5/21/2017 12:10:14 AM ******/
CREATE TABLE [dbo].[ChiTietBanHang](
	[id_ChiTietBH] [int] IDENTITY(1,1) NOT NULL,
	[Id_HangHoa] [int] NULL,
	[id_ChungTuXuat] [int] NULL,
	[id_donvi] [int] NULL,
	[SoLuong] [int] NULL,
	[DonGia] [int] NULL,
	[ThanhTien] [money] NULL,
	[ChietKhau] [int] NULL,
 CONSTRAINT [PK_ChiTietBanHang] PRIMARY KEY (id_ChiTietBH))
/****** Object:  Table [dbo].[ChiTietNhapHang]    Script Date: 5/21/2017 12:10:14 AM ******/
CREATE TABLE [dbo].[ChiTietNhapHang](
	[id_CTNhapHang] [int] IDENTITY(1,1) NOT NULL,
	[id_HangHoa] [int] NULL,
	[id_ChungTuNhap] [int] NULL,
	[id_DonVi] [int] NULL,
	[SoLuong] [int] NULL,
	[DonGia] [int] NULL,
	[ThanhTien] [money] NULL,
	[GhiChu] [nvarchar](1000) NULL,
 CONSTRAINT [PK_ChiTietNhapHang] PRIMARY KEY (id_CTNhapHang))
/****** Object:  Table [dbo].[ChucNang]    Script Date: 5/21/2017 12:10:14 AM ******/
CREATE TABLE [dbo].[ChucNang](
	[id_ChucNang] [int] IDENTITY(1,1) NOT NULL,
	[TenChucNang] [nvarchar](100) NULL,
	[id_Cha] [int] NULL,
 CONSTRAINT [PK_ChucNang] PRIMARY KEY (id_ChucNang))
/****** Object:  Table [dbo].[ChuyenKho]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[ChuyenKho](
	[id_ChuyenKho] [int] IDENTITY(1,1) NOT NULL,
	[id_Kho] [int] NULL,
	[Khoxuathang] [nvarchar](100) NULL,
	[KhoNhapHang] [nvarchar](100) NULL,
	[NguoiGui] [nvarchar](50) NULL,
	[NguoiNhan] [nvarchar](50) NULL,
	[Date] [datetime] NULL CONSTRAINT [DF_ChuyenKho_Date]  DEFAULT (getdate()),
 CONSTRAINT [PK_ChuyenKho] PRIMARY KEY (id_ChuyenKho))
/****** Object:  Table [dbo].[DonViTinh]    Script Date: 5/21/2017 12:10:14 AM ******/
CREATE TABLE [dbo].[DonViTinh](
	[Id_DV] [int] IDENTITY(1,1) NOT NULL,
	[TenDonVi] [nvarchar](100) NULL,
	[GhiChu] [nvarchar](100) NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_DonViTinh] PRIMARY KEY (Id_DV))
/****** Object:  Table [dbo].[HangHoa]    Script Date: 5/21/2017 12:10:14 AM ******/
CREATE TABLE [dbo].[HangHoa](
	[id_hanghoa] [int] IDENTITY(1,1) NOT NULL,
	[id_nhomhang] [int] NULL,
	[Tenhang] [nvarchar](100) NULL,
	[id_DonVi] [int] NULL,
	[GiaMua] [money] NULL,
	[GiaBanSi] [money] NULL,
	[GiaBanLe] [money] NULL,
	[ToiThieu] [int] NULL,
	[TinhChat] [nvarchar](50) NULL,
	[id_kho] [int] NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_HangHoa] PRIMARY KEY (id_hanghoa))
/****** Object:  Table [dbo].[KhachHang]    Script Date: 5/21/2017 12:10:14 AM ******/
CREATE TABLE [dbo].[KhachHang](
	[Id_KH] [int] IDENTITY(1,1) NOT NULL,
	[Id_KhuVuc] [int] NULL,
	[TenKhachHang] [nvarchar](100) NULL,
	[DiaChi] [nvarchar](100) NULL,
	[SĐT] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Website] [nvarchar](50) NULL,
	[MaSoThue] [int] NULL,
	[SoTK] [int] NULL,
	[TenNganHang] [nvarchar](50) NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY (Id_KH))
/****** Object:  Table [dbo].[Kho]    Script Date: 5/21/2017 12:10:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kho](
	[Id_Kho] [int] IDENTITY(1,1) NOT NULL,
	[TenKho] [nvarchar](100) NULL,
	[DiaChi] [nvarchar](100) NULL,
	[SDT] [nvarchar](50) NULL,
	[GhiChu] [nvarchar](1000) NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_Kho] PRIMARY KEY (Id_Kho))
/****** Object:  Table [dbo].[KhuVuc]    Script Date: 5/21/2017 12:10:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhuVuc](
	[Id_KhuVuc] [int] IDENTITY(1,1) NOT NULL,
	[TenKhuVuc] [nvarchar](100) NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_KhuVuc] PRIMARY KEY (Id_KhuVuc))
/****** Object:  Table [dbo].[LienHe]    Script Date: 5/21/2017 12:10:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LienHe](
	[TenDonVi] [nvarchar](100) NULL,
	[DiaChi] [nvarchar](100) NULL,
	[SDT] [int] NULL,
	[Fax] [int] NULL,
	[website] [nvarchar](100) NULL,
	[mail] [nvarchar](100) NULL,
	[linhvuc] [nvarchar](100) NULL,
	[nganhnghe] [nvarchar](100) NULL,
	[Id_Thue] [int] NULL,
	[GPKD] [int] NULL,
	[NguoiLienHe] [nvarchar](100) NULL,
	[NoiDung] [nvarchar](1000) NULL
) 


/****** Object:  Table [dbo].[MaVach]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[MaVach](
	[id_MaVach] [int] IDENTITY(1,1) NOT NULL,
	[id_Hang] [int] NULL,
	[TenTiengViet] [nvarchar](100) NULL,
	[Gia] [money] NULL,
 CONSTRAINT [PK_MaVach] PRIMARY KEY (id_MaVach))
/****** Object:  Table [dbo].[NguoiDung]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[NguoiDung](
	[id_NguoiDung] [int] IDENTITY(1,1) NOT NULL,
	[TenNguoiDung] [nvarchar](100) NULL,
	[id_VaiTro] [int] NULL,
	[Username] [nvarchar](100) NULL,
	[Password] [nvarchar](100) NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_NguoiDung] PRIMARY KEY (id_NguoiDung))
/****** Object:  Table [dbo].[NhaCungCap]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[NhaCungCap](
	[id_NCC] [int] IDENTITY(1,1) NOT NULL,
	[id_KV] [int] NULL,
	[TenNCC] [nvarchar](100) NULL,
	[ChucVu] [nvarchar](100) NULL,
	[DiaChi] [nvarchar](100) NULL,
	[SDT] [nvarchar](50) NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_NhaCungCap] PRIMARY KEY (id_NCC))
/****** Object:  Table [dbo].[NhanVien]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[NhanVien](
	[id_NhanVien] [int] IDENTITY(1,1) NOT NULL,
	[TenNV] [nvarchar](100) NULL,
	[DiaChi] [nvarchar](100) NULL,
	[SDT] [nvarchar](50) NULL,
	[email] [nvarchar](100) NULL,
	[trangthai] [int] NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY (id_NhanVien))
/****** Object:  Table [dbo].[NhapHang]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[NhapHang](
	[id_Kho] [int] IDENTITY(1,1) NOT NULL,
	[id_ChungTuNhap] [int] NOT NULL,
	[id_NhaCungCap] [int] NULL,
	[NgayNhap] [datetime] NULL CONSTRAINT [DF_NhapHang_NgayNhap]  DEFAULT (getdate()),
	[SoPhieuVietTay] [int] NULL,
	[SoVat] [int] NULL,
	[SoTien] [money] NULL,
	[ChietKhau] [int] NULL,
	[Vat] [int] NULL,
	[ThanhToan] [money] NULL,
 CONSTRAINT [PK_NhapHang] PRIMARY KEY (id_ChungTuNhap))
/****** Object:  Table [dbo].[NhatKy]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[NhatKy](
	[STT] [int] IDENTITY(1,1) NOT NULL,
	[id_NguoiDung] [int] NULL,
	[MayTinh] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
 CONSTRAINT [PK_NhatKy] PRIMARY KEY (STT))
/****** Object:  Table [dbo].[NhomHang]    Script Date: 5/21/2017 12:10:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhomHang](
	[Id_NhomHang] [int] IDENTITY(1,1) NOT NULL,
	[TenNhomHang] [nvarchar](50) NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_NhomHang] PRIMARY KEY (Id_NhomHang))
/****** Object:  Table [dbo].[ThuTien]    Script Date: 5/21/2017 12:10:14 AM ******/
CREATE TABLE [dbo].[ThuTien](
	[id_ChungTuThuTien] [int] IDENTITY(1,1) NOT NULL,
	[id_KhuVuc] [int] NULL,
	[Ngay] [datetime] NULL CONSTRAINT [DF_ThuTien_Ngay]  DEFAULT (getdate()),
	[id_KhachHang] [int] NULL,
	[TenKH] [nvarchar](100) NULL,
	[SoTien] [money] NULL,
	[DaTra] [money] NULL,
	[ConLai] [money] NULL,
 CONSTRAINT [PK_ThuTien] PRIMARY KEY (id_ChungTuThuTien))
/****** Object:  Table [dbo].[TonKho]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[TonKho](
	[stt] [int] IDENTITY(1,1) NOT NULL,
	[id_Kho] [int] NULL,
	[id_MaHang] [int] NULL,
	[TenHang] [nvarchar](100) NULL,
	[id_donvi] [int] NULL,
	[soluong] [int] NULL,
	[dongia] [int] NULL,
	[id_NhomHang] [int] NULL,
	[ThanhTien] [money] NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_TonKho] PRIMARY KEY (stt))
/****** Object:  Table [dbo].[TyGia]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[TyGia](
	[id_TyGia] [nvarchar](50) NOT NULL,
	[TenTyGia] [nvarchar](100) NULL,
	[TyGiaQuyDoi] [money] NULL,
	[TrangThai] [int] NULL,
 CONSTRAINT [PK_TyGia] PRIMARY KEY (id_TyGia))
/****** Object:  Table [dbo].[VaiTro]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[VaiTro](
	[idVaiTro] [int] IDENTITY(1,1) NOT NULL,
	[TenVaiTro] [nvarchar](100) NULL,
	[MoTa] [nvarchar](1028) NULL,
 CONSTRAINT [PK_VaiTro] PRIMARY KEY (idVaiTro))
/****** Object:  Table [dbo].[VaiTro_ChucNang]    Script Date: 5/21/2017 12:10:14 AM ******/

CREATE TABLE [dbo].[VaiTro_ChucNang](
	[id_VT_CN] [int] NULL,
	[id_VaiTro] [int] NOT NULL,
	[id_ChucNang] [int] NOT NULL,
	[Them] [bit] NULL,
	[Xoa] [bit] NULL,
	[Sua] [bit] NULL,
	[CapNhat] [bit] NULL,
 CONSTRAINT [PK_VaiTro_ChucNang_1] PRIMARY KEY (id_VaiTro,id_ChucNang))
SET IDENTITY_INSERT [dbo].[BanHang] ON 

INSERT [dbo].[BanHang] ([Id_kho], [id_ChungTuban], [id_khachhang], [NgayBan], [SoPhieuVietTay], [SoTien], [ChieuKhau], [Vat], [ThanhToan]) VALUES (4, 1, 1, CAST(N'2017-05-20 23:25:34.687' AS DateTime), 5478972, 18.9000, 5, 87912, 20.0000)
INSERT [dbo].[BanHang] ([Id_kho], [id_ChungTuban], [id_khachhang], [NgayBan], [SoPhieuVietTay], [SoTien], [ChieuKhau], [Vat], [ThanhToan]) VALUES (1, 2, 2, CAST(N'2017-05-20 23:32:02.260' AS DateTime), 5481231, 100.0000, 4, 54651, 10.5000)
INSERT [dbo].[BanHang] ([Id_kho], [id_ChungTuban], [id_khachhang], [NgayBan], [SoPhieuVietTay], [SoTien], [ChieuKhau], [Vat], [ThanhToan]) VALUES (2, 3, 3, CAST(N'2017-05-20 23:33:45.220' AS DateTime), 8723112, 12.0000, 1, 58713, 12.5000)
SET IDENTITY_INSERT [dbo].[BanHang] OFF
SET IDENTITY_INSERT [dbo].[BoPhan] ON 

INSERT [dbo].[BoPhan] ([id_BoPhan], [TenBoPhan], [GhiChu], [TrangThai]) VALUES (1, N'Giám Đốc', N'Điêu hành mọi hoạt động', 1)
INSERT [dbo].[BoPhan] ([id_BoPhan], [TenBoPhan], [GhiChu], [TrangThai]) VALUES (2, N'Kinh Doanh', N'Mua Bán Sản Phẩm', 1)
INSERT [dbo].[BoPhan] ([id_BoPhan], [TenBoPhan], [GhiChu], [TrangThai]) VALUES (3, N'Kế Toán', N'Thống Kê Doanh Thu', 1)
INSERT [dbo].[BoPhan] ([id_BoPhan], [TenBoPhan], [GhiChu], [TrangThai]) VALUES (4, N'Kỹ Thuật', N'Sửa chửa chương trình', 1)
SET IDENTITY_INSERT [dbo].[BoPhan] OFF
SET IDENTITY_INSERT [dbo].[ChiTien] ON 

INSERT [dbo].[ChiTien] ([id_ChungTuChiTien], [id_KhuVuc], [Ngay], [id_NCC], [Ten_NCC], [SoTien], [DaTra], [ConLai]) VALUES (1, 1, CAST(N'2017-05-21 00:01:05.463' AS DateTime), 1, N'Bách Vệt', 8900.0000, 8000.0000, 900.0000)
INSERT [dbo].[ChiTien] ([id_ChungTuChiTien], [id_KhuVuc], [Ngay], [id_NCC], [Ten_NCC], [SoTien], [DaTra], [ConLai]) VALUES (2, 2, CAST(N'2017-05-21 00:01:08.060' AS DateTime), 2, N'Đinh Tị', 7800.0000, 4000.0000, 3800.0000)
INSERT [dbo].[ChiTien] ([id_ChungTuChiTien], [id_KhuVuc], [Ngay], [id_NCC], [Ten_NCC], [SoTien], [DaTra], [ConLai]) VALUES (3, 3, CAST(N'2017-05-21 00:01:12.697' AS DateTime), 3, N'Nhã Nam', 1900.0000, 1000.0000, 900.0000)
SET IDENTITY_INSERT [dbo].[ChiTien] OFF
SET IDENTITY_INSERT [dbo].[ChiTietBanHang] ON 

INSERT [dbo].[ChiTietBanHang] ([id_ChiTietBH], [Id_HangHoa], [id_ChungTuXuat], [id_donvi], [SoLuong], [DonGia], [ThanhTien], [ChietKhau]) VALUES (1, 1, 1, 1, 3, 1, 20.0000, 5)
INSERT [dbo].[ChiTietBanHang] ([id_ChiTietBH], [Id_HangHoa], [id_ChungTuXuat], [id_donvi], [SoLuong], [DonGia], [ThanhTien], [ChietKhau]) VALUES (2, 2, 2, 5, 1, 1, 10.5000, 4)
INSERT [dbo].[ChiTietBanHang] ([id_ChiTietBH], [Id_HangHoa], [id_ChungTuXuat], [id_donvi], [SoLuong], [DonGia], [ThanhTien], [ChietKhau]) VALUES (3, 4, 3, 1, 2, 1, 12.5000, 1)
SET IDENTITY_INSERT [dbo].[ChiTietBanHang] OFF
SET IDENTITY_INSERT [dbo].[ChiTietNhapHang] ON 

INSERT [dbo].[ChiTietNhapHang] ([id_CTNhapHang], [id_HangHoa], [id_ChungTuNhap], [id_DonVi], [SoLuong], [DonGia], [ThanhTien], [GhiChu]) VALUES (1, 1, 1, 1, 50, 1, 8.9000, N'Sách Truyện')
INSERT [dbo].[ChiTietNhapHang] ([id_CTNhapHang], [id_HangHoa], [id_ChungTuNhap], [id_DonVi], [SoLuong], [DonGia], [ThanhTien], [GhiChu]) VALUES (2, 2, 2, 5, 50, 1, 12.8000, N'Bình Đựng Nước')
INSERT [dbo].[ChiTietNhapHang] ([id_CTNhapHang], [id_HangHoa], [id_ChungTuNhap], [id_DonVi], [SoLuong], [DonGia], [ThanhTien], [GhiChu]) VALUES (3, 4, 3, 1, 50, 1, 7.5000, N'Sách Truyện')
INSERT [dbo].[ChiTietNhapHang] ([id_CTNhapHang], [id_HangHoa], [id_ChungTuNhap], [id_DonVi], [SoLuong], [DonGia], [ThanhTien], [GhiChu]) VALUES (4, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ChiTietNhapHang] OFF
SET IDENTITY_INSERT [dbo].[ChucNang] ON 

INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (1, N'Hệ Thống', NULL)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (2, N'Danh Mục', NULL)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (3, N'Chức Năng', NULL)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (4, N'Hệ Thống', 1)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (5, N'Bảo Mật', 1)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (6, N'Dữ Liệu', 1)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (7, N'Đơn Vị', 4)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (8, N'Quản Lý Phân Quyền', 5)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (9, N'Đổi Mật Khẩu', 5)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (10, N'Nhật Ký Hệ Thống', 5)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (11, N'Quản Lý Người Dùng ', 8)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (12, N'Vai Trò và Quyền Hạn', 8)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (13, N'Sao Lưu', 6)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (14, N'Phục Hồi', 6)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (15, N'Sửa Chửa', 6)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (16, N'Kết Chuyển', 6)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (17, N'Đối Tác ', 2)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (18, N'Kho Hàng', 2)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (19, N'Bộ Phận', 2)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (20, N'Khu Vực', 17)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (21, N'Khách Hàng', 17)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (22, N'Nhà Phân Phối', 17)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (23, N'Kho', 18)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (24, N'Đơn Vị', 18)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (25, N'Nhóm Hàng', 18)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (26, N'Hàng Hóa', 18)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (27, N'In Mã Vạch', 18)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (28, N'Tỷ Giá ', 18)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (29, N'Quy Đổi Đơn Vị', 18)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (30, N'Bộ Phận', 19)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (31, N'Nhân Viên', 19)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (32, N'Bán Hàng', 3)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (33, N'Công Nợ', 3)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (34, N'Kho Hàng', 3)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (35, N'Tiện Ích', 3)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (36, N'Hóa Đơn', 3)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (37, N'Báo Cáo', 3)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (38, N'Công Cụ', 3)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (39, N'Mua Hàng', 32)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (40, N'Bán Hàng', 32)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (41, N'Nhập Trả Hàng', 32)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (42, N'Xuất Trả Hàng', 32)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (43, N'Thu Tiền ', 33)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (44, N'Trả Tiền', 33)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (45, N'Nhập Kho', 34)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (46, N'Xuất Kho', 34)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (47, N'Tồn Kho', 34)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (48, N'Đóng Gói', 35)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (49, N'Kiểm Kê', 35)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (50, N'Chuyển Kho', 35)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (51, N'Tổng Hợp Tồn Kho', 35)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (52, N'Hóa Đơn', 36)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (53, N'Quản Lý Chứng Từ', 36)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (54, N'Báo Cáo Kho', 37)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (55, N'Doanh Thu Bán Hàng', 37)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (56, N'Hạn Sử Dụng', 37)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (57, N'Đặt Hàng', 38)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (58, N'Nhập Số Dư Ban Đầu', 38)
INSERT [dbo].[ChucNang] ([id_ChucNang], [TenChucNang], [id_Cha]) VALUES (59, N'Lịch Sử Hàng Hóa', 38)
SET IDENTITY_INSERT [dbo].[ChucNang] OFF
SET IDENTITY_INSERT [dbo].[ChuyenKho] ON 

INSERT [dbo].[ChuyenKho] ([id_ChuyenKho], [id_Kho], [Khoxuathang], [KhoNhapHang], [NguoiGui], [NguoiNhan], [Date]) VALUES (1, 1, N'2', N'1', N'Quản Lý', N'Quản Lý', CAST(N'2017-05-20 23:55:11.490' AS DateTime))
INSERT [dbo].[ChuyenKho] ([id_ChuyenKho], [id_Kho], [Khoxuathang], [KhoNhapHang], [NguoiGui], [NguoiNhan], [Date]) VALUES (2, 3, N'4', N'3', N'Thư Ký', N'Thư Ký', CAST(N'2017-05-20 23:55:25.073' AS DateTime))
INSERT [dbo].[ChuyenKho] ([id_ChuyenKho], [id_Kho], [Khoxuathang], [KhoNhapHang], [NguoiGui], [NguoiNhan], [Date]) VALUES (3, 5, N'3', N'5', N'Nhân Viên', N'Nhân Viên', CAST(N'2017-05-20 23:56:54.373' AS DateTime))
INSERT [dbo].[ChuyenKho] ([id_ChuyenKho], [id_Kho], [Khoxuathang], [KhoNhapHang], [NguoiGui], [NguoiNhan], [Date]) VALUES (4, 2, N'5', N'2', N'Quản Lý', N'Nhân Viên', CAST(N'2017-05-20 23:57:15.890' AS DateTime))
SET IDENTITY_INSERT [dbo].[ChuyenKho] OFF
SET IDENTITY_INSERT [dbo].[DonViTinh] ON 

INSERT [dbo].[DonViTinh] ([Id_DV], [TenDonVi], [GhiChu], [TrangThai]) VALUES (1, N'Cuốn', N'Trên 1 sản phẩm', 1)
INSERT [dbo].[DonViTinh] ([Id_DV], [TenDonVi], [GhiChu], [TrangThai]) VALUES (2, N'Tờ', N'Trên 1 sản phẩm', 1)
INSERT [dbo].[DonViTinh] ([Id_DV], [TenDonVi], [GhiChu], [TrangThai]) VALUES (3, N'Cái', N'Trên 1 sản phẩm', 1)
INSERT [dbo].[DonViTinh] ([Id_DV], [TenDonVi], [GhiChu], [TrangThai]) VALUES (4, N'Cây', N'Trên 1 sản phẩm', 1)
INSERT [dbo].[DonViTinh] ([Id_DV], [TenDonVi], [GhiChu], [TrangThai]) VALUES (5, N'Chai', N'Trên 1 sản phẩm', 1)
SET IDENTITY_INSERT [dbo].[DonViTinh] OFF
SET IDENTITY_INSERT [dbo].[HangHoa] ON 

INSERT [dbo].[HangHoa] ([id_hanghoa], [id_nhomhang], [Tenhang], [id_DonVi], [GiaMua], [GiaBanSi], [GiaBanLe], [ToiThieu], [TinhChat], [id_kho], [TrangThai]) VALUES (1, 2, N'Tam Sinh Tam The', 1, 89.0000, 50.0000, 89.0000, 50, N'Hàng Hóa', 4, 1)
INSERT [dbo].[HangHoa] ([id_hanghoa], [id_nhomhang], [Tenhang], [id_DonVi], [GiaMua], [GiaBanSi], [GiaBanLe], [ToiThieu], [TinhChat], [id_kho], [TrangThai]) VALUES (2, 3, N'Lock and Lock', 5, 128.0000, 100.0000, 128.0000, 40, N'Hàng Hóa', 5, 1)
INSERT [dbo].[HangHoa] ([id_hanghoa], [id_nhomhang], [Tenhang], [id_DonVi], [GiaMua], [GiaBanSi], [GiaBanLe], [ToiThieu], [TinhChat], [id_kho], [TrangThai]) VALUES (3, 4, N'Bài Ma Thuật', 3, 100.0000, 75.0000, 100.0000, 80, N'Hàng Hóa', 1, 1)
INSERT [dbo].[HangHoa] ([id_hanghoa], [id_nhomhang], [Tenhang], [id_DonVi], [GiaMua], [GiaBanSi], [GiaBanLe], [ToiThieu], [TinhChat], [id_kho], [TrangThai]) VALUES (4, 2, N'Chẩm Thượng Thư', 1, 75.0000, 45.0000, 75.0000, 20, N'Hàng Hóa', 2, 1)
SET IDENTITY_INSERT [dbo].[HangHoa] OFF
SET IDENTITY_INSERT [dbo].[KhachHang] ON 

INSERT [dbo].[KhachHang] ([Id_KH], [Id_KhuVuc], [TenKhachHang], [DiaChi], [SĐT], [Email], [Website], [MaSoThue], [SoTK], [TenNganHang], [TrangThai]) VALUES (1, 1, N'Lê Thanh Hằng', N'651/8 Phố Hàng Bún', N'0126417593', N'lethanhhang@gmail.com', N'thanhhang.com', 12365474, 567849512, N'BD Bank', 1)
INSERT [dbo].[KhachHang] ([Id_KH], [Id_KhuVuc], [TenKhachHang], [DiaChi], [SĐT], [Email], [Website], [MaSoThue], [SoTK], [TenNganHang], [TrangThai]) VALUES (2, 2, N'Trần Công Toản', N'43 Nguyễn Chi Thanh', N'0903651745', N'trancongtoan@gmail.com', N'congtoan.com', 99874451, 567423571, N'BD Bank', 1)
INSERT [dbo].[KhachHang] ([Id_KH], [Id_KhuVuc], [TenKhachHang], [DiaChi], [SĐT], [Email], [Website], [MaSoThue], [SoTK], [TenNganHang], [TrangThai]) VALUES (3, 3, N'Võ Mai Thắm', N'49 Vườn Trầu ', N'0631547821', N'ThamHaHa@gmail.com', N'maitham.com', 88774512, 541789233, N'BD Bank', 1)
SET IDENTITY_INSERT [dbo].[KhachHang] OFF
SET IDENTITY_INSERT [dbo].[Kho] ON 

INSERT [dbo].[Kho] ([Id_Kho], [TenKho], [DiaChi], [SDT], [GhiChu], [TrangThai]) VALUES (1, N'Kho Hà Nội', N'45 Sư Vạn Hạnh Hà Nội', N'0123548412', N'Kho chứa hàng hóa ở Hà Nội', 1)
INSERT [dbo].[Kho] ([Id_Kho], [TenKho], [DiaChi], [SDT], [GhiChu], [TrangThai]) VALUES (2, N'Kho Thừa Thiên', N'54 Vinh Xuân Huế', N'0154681238', N'Kho chứa hàng khu vực miền Trung', 1)
INSERT [dbo].[Kho] ([Id_Kho], [TenKho], [DiaChi], [SDT], [GhiChu], [TrangThai]) VALUES (3, N'Kho Khánh Hòa', N'46 Khánh Hòa', N'0123215132', N'Kho chứa hàng miền Trung 2', 1)
INSERT [dbo].[Kho] ([Id_Kho], [TenKho], [DiaChi], [SDT], [GhiChu], [TrangThai]) VALUES (4, N'Kho Cần Giờ', N'6 Lê Lợi', N'0123412318', N'Kho chứa hàng miền Nam', 1)
INSERT [dbo].[Kho] ([Id_Kho], [TenKho], [DiaChi], [SDT], [GhiChu], [TrangThai]) VALUES (5, N'Kho HCM', N'227 Nguyễn Văn Cừ Q5', N'0123185113', N'Kho Chứa Hàng Miền Nam', 1)
INSERT [dbo].[Kho] ([Id_Kho], [TenKho], [DiaChi], [SDT], [GhiChu], [TrangThai]) VALUES (6, N'Cao Bằng', N'54 Cao Bằng', N'0213843218', N'Kkho Chứa Hàng miền Bắc', 1)
SET IDENTITY_INSERT [dbo].[Kho] OFF
SET IDENTITY_INSERT [dbo].[KhuVuc] ON 

INSERT [dbo].[KhuVuc] ([Id_KhuVuc], [TenKhuVuc], [TrangThai]) VALUES (1, N'Miền Bắc', 1)
INSERT [dbo].[KhuVuc] ([Id_KhuVuc], [TenKhuVuc], [TrangThai]) VALUES (2, N'Miền Trung', 2)
INSERT [dbo].[KhuVuc] ([Id_KhuVuc], [TenKhuVuc], [TrangThai]) VALUES (3, N'Miền Nam', 3)
SET IDENTITY_INSERT [dbo].[KhuVuc] OFF
INSERT [dbo].[LienHe] ([TenDonVi], [DiaChi], [SDT], [Fax], [website], [mail], [linhvuc], [nganhnghe], [Id_Thue], [GPKD], [NguoiLienHe], [NoiDung]) VALUES (N'Công Ty TNHH 1 thành viên Gia Đình Loạn Thi', N'277 Nguyễn Văn Cừ', 932736009, 932736009, N'giadinhvuive.com', N'hohoangnhu1996@gmail.com', N'2.Thương Mại', N'Cung Cấp Giải Pháp Phần Mền', 1100803080, 1100803080, N'Nguyễn Thành Tiến', NULL)
SET IDENTITY_INSERT [dbo].[MaVach] ON 

INSERT [dbo].[MaVach] ([id_MaVach], [id_Hang], [TenTiengViet], [Gia]) VALUES (1, 1, N'In Trên Sản Phẩm', 0.0000)
INSERT [dbo].[MaVach] ([id_MaVach], [id_Hang], [TenTiengViet], [Gia]) VALUES (2, 2, N'Từ  Giấy 4x4', 0.0000)
INSERT [dbo].[MaVach] ([id_MaVach], [id_Hang], [TenTiengViet], [Gia]) VALUES (3, 3, N'Từ Giấy 4x4', 0.0000)
INSERT [dbo].[MaVach] ([id_MaVach], [id_Hang], [TenTiengViet], [Gia]) VALUES (4, 4, N'In Trên Sản Pham', 0.0000)
SET IDENTITY_INSERT [dbo].[MaVach] OFF
SET IDENTITY_INSERT [dbo].[NguoiDung] ON 

INSERT [dbo].[NguoiDung] ([id_NguoiDung], [TenNguoiDung], [id_VaiTro], [Username], [Password], [TrangThai]) VALUES (1, N'admin', 1, N'admin', N'123456', 1)
INSERT [dbo].[NguoiDung] ([id_NguoiDung], [TenNguoiDung], [id_VaiTro], [Username], [Password], [TrangThai]) VALUES (2, N'salers', 2, N'salers', N'123456', 1)
SET IDENTITY_INSERT [dbo].[NguoiDung] OFF
SET IDENTITY_INSERT [dbo].[NhaCungCap] ON 

INSERT [dbo].[NhaCungCap] ([id_NCC], [id_KV], [TenNCC], [ChucVu], [DiaChi], [SDT], [TrangThai]) VALUES (1, 1, N'Bách Việt', N'Giám Đốc', N' 69 Cầu Giấy Hà Nội', N'0127841264', 1)
INSERT [dbo].[NhaCungCap] ([id_NCC], [id_KV], [TenNCC], [ChucVu], [DiaChi], [SDT], [TrangThai]) VALUES (2, 2, N'Đinh Tị', N'Trưởng Phòng', N'21 Hà Đông Quảng Trị', N'0123648851', 1)
INSERT [dbo].[NhaCungCap] ([id_NCC], [id_KV], [TenNCC], [ChucVu], [DiaChi], [SDT], [TrangThai]) VALUES (3, 3, N'Nhã Nam', N'Phó Giám Đốc', N'65 Lê Quang Định', N'0642135484', 1)
SET IDENTITY_INSERT [dbo].[NhaCungCap] OFF
SET IDENTITY_INSERT [dbo].[NhanVien] ON 

INSERT [dbo].[NhanVien] ([id_NhanVien], [TenNV], [DiaChi], [SDT], [email], [trangthai]) VALUES (1, N'Hồ Hoàng Nhu', N'577 B Hậu Giang P11 Q6', N'01285171578', N'hohoangnhu1996@gmail.com', 1)
INSERT [dbo].[NhanVien] ([id_NhanVien], [TenNV], [DiaChi], [SDT], [email], [trangthai]) VALUES (2, N'Nguyễn Thành Tiến', N'21 Hàng Xanh', N'0932736009', N'1460987@gmail.com', 1)
INSERT [dbo].[NhanVien] ([id_NhanVien], [TenNV], [DiaChi], [SDT], [email], [trangthai]) VALUES (3, N'Võ Mai Thắm', N'45 Hàng Bún', N'0978231584', N'1460917@gmail.com', 1)
INSERT [dbo].[NhanVien] ([id_NhanVien], [TenNV], [DiaChi], [SDT], [email], [trangthai]) VALUES (4, N'Phạm Ngọc Hương', N'69 Hồ Xuân Hương', N'0978782354', N'1460440@gmail.com', 1)
INSERT [dbo].[NhanVien] ([id_NhanVien], [TenNV], [DiaChi], [SDT], [email], [trangthai]) VALUES (5, N'Lê Văn Chiu', N'96 Vĩnh Xuân', N'0985912359', N'1460146@gmail.com', 1)
SET IDENTITY_INSERT [dbo].[NhanVien] OFF
SET IDENTITY_INSERT [dbo].[NhapHang] ON 

INSERT [dbo].[NhapHang] ([id_Kho], [id_ChungTuNhap], [id_NhaCungCap], [NgayNhap], [SoPhieuVietTay], [SoVat], [SoTien], [ChietKhau], [Vat], [ThanhToan]) VALUES (1, 1, 1, CAST(N'2017-05-20 22:54:40.247' AS DateTime), 4587132, 8721381, 6900.0000, 5, 4587132, 5900.0000)
INSERT [dbo].[NhapHang] ([id_Kho], [id_ChungTuNhap], [id_NhaCungCap], [NgayNhap], [SoPhieuVietTay], [SoVat], [SoTien], [ChietKhau], [Vat], [ThanhToan]) VALUES (2, 2, 2, CAST(N'2017-05-20 22:55:03.410' AS DateTime), 8547123, 8578315, 5000.0000, 4, 4853158, 4500.0000)
INSERT [dbo].[NhapHang] ([id_Kho], [id_ChungTuNhap], [id_NhaCungCap], [NgayNhap], [SoPhieuVietTay], [SoVat], [SoTien], [ChietKhau], [Vat], [ThanhToan]) VALUES (3, 3, 3, CAST(N'2017-05-20 22:56:33.027' AS DateTime), 5413818, 5753158, 6400.0000, 6, 5231878, 6000.0000)
SET IDENTITY_INSERT [dbo].[NhapHang] OFF
SET IDENTITY_INSERT [dbo].[NhomHang] ON 

INSERT [dbo].[NhomHang] ([Id_NhomHang], [TenNhomHang], [TrangThai]) VALUES (1, N'Hàng Cũ', 1)
INSERT [dbo].[NhomHang] ([Id_NhomHang], [TenNhomHang], [TrangThai]) VALUES (2, N'Hàng Mới', 1)
INSERT [dbo].[NhomHang] ([Id_NhomHang], [TenNhomHang], [TrangThai]) VALUES (3, N'Hàng Bán Chạy', 1)
INSERT [dbo].[NhomHang] ([Id_NhomHang], [TenNhomHang], [TrangThai]) VALUES (4, N'Hàng Chất', 1)
SET IDENTITY_INSERT [dbo].[NhomHang] OFF
SET IDENTITY_INSERT [dbo].[ThuTien] ON 

INSERT [dbo].[ThuTien] ([id_ChungTuThuTien], [id_KhuVuc], [Ngay], [id_KhachHang], [TenKH], [SoTien], [DaTra], [ConLai]) VALUES (1, 1, CAST(N'2017-05-20 23:58:58.360' AS DateTime), 1, N'Lê Thanh Hằng', 20.0000, 20.0000, 0.0000)
INSERT [dbo].[ThuTien] ([id_ChungTuThuTien], [id_KhuVuc], [Ngay], [id_KhachHang], [TenKH], [SoTien], [DaTra], [ConLai]) VALUES (2, 2, CAST(N'2017-05-20 23:59:40.810' AS DateTime), 2, N'Trần Công Toản', 10.5000, 10.5000, 0.0000)
INSERT [dbo].[ThuTien] ([id_ChungTuThuTien], [id_KhuVuc], [Ngay], [id_KhachHang], [TenKH], [SoTien], [DaTra], [ConLai]) VALUES (3, 3, CAST(N'2017-05-21 00:00:33.820' AS DateTime), 3, N'Võ Mai Thắm', 12.5000, 12.5000, 0.0000)
SET IDENTITY_INSERT [dbo].[ThuTien] OFF
SET IDENTITY_INSERT [dbo].[TonKho] ON 

INSERT [dbo].[TonKho] ([stt], [id_Kho], [id_MaHang], [TenHang], [id_donvi], [soluong], [dongia], [id_NhomHang], [ThanhTien], [TrangThai]) VALUES (1, 4, 1, N'Tam Sinh Tam Thế', 1, 30, NULL, 2, 3000.0000, 1)
INSERT [dbo].[TonKho] ([stt], [id_Kho], [id_MaHang], [TenHang], [id_donvi], [soluong], [dongia], [id_NhomHang], [ThanhTien], [TrangThai]) VALUES (2, 5, 2, N'Lock and Lock', 5, 30, NULL, 3, 2500.0000, 1)
INSERT [dbo].[TonKho] ([stt], [id_Kho], [id_MaHang], [TenHang], [id_donvi], [soluong], [dongia], [id_NhomHang], [ThanhTien], [TrangThai]) VALUES (3, 1, 3, N'Bài Ma Thuật', 3, 30, NULL, 4, 2900.0000, 1)
INSERT [dbo].[TonKho] ([stt], [id_Kho], [id_MaHang], [TenHang], [id_donvi], [soluong], [dongia], [id_NhomHang], [ThanhTien], [TrangThai]) VALUES (4, 2, 4, N'Chẩm Thượng Thư', 1, 30, NULL, 2, 2100.0000, 1)
SET IDENTITY_INSERT [dbo].[TonKho] OFF
INSERT [dbo].[TyGia] ([id_TyGia], [TenTyGia], [TyGiaQuyDoi], [TrangThai]) VALUES (N'EUR', N'Tiền Dùng Chung EU', 19.4570, 1)
INSERT [dbo].[TyGia] ([id_TyGia], [TenTyGia], [TyGiaQuyDoi], [TrangThai]) VALUES (N'GBP', N'Bảng Anh', 26.1490, 1)
INSERT [dbo].[TyGia] ([id_TyGia], [TenTyGia], [TyGiaQuyDoi], [TrangThai]) VALUES (N'USD', N'Đô La', 23.0000, 1)
INSERT [dbo].[TyGia] ([id_TyGia], [TenTyGia], [TyGiaQuyDoi], [TrangThai]) VALUES (N'VND', N'Việt Nam Đồng', 1.0000, 1)
SET IDENTITY_INSERT [dbo].[VaiTro] ON 

INSERT [dbo].[VaiTro] ([idVaiTro], [TenVaiTro], [MoTa]) VALUES (1, N'Quản Lý Hệ Thống', N'Quản Trị Hệ Thống. Chịu Trách Nhiệm Toàn Bộ Hệ Thống')
INSERT [dbo].[VaiTro] ([idVaiTro], [TenVaiTro], [MoTa]) VALUES (2, N'Nhân Viên', N'Xem chức năng, Quản lý mua Bán Hàng')
SET IDENTITY_INSERT [dbo].[VaiTro] OFF
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (1, 1, 1, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (2, 1, 2, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (3, 1, 3, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (4, 1, 4, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (5, 1, 5, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (6, 1, 6, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (7, 1, 7, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (8, 1, 8, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (9, 1, 9, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (10, 1, 10, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (11, 1, 11, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (12, 1, 12, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (13, 1, 13, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (14, 1, 14, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (15, 1, 15, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (16, 1, 16, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (17, 1, 17, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (18, 1, 18, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (19, 1, 19, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (20, 1, 20, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (21, 1, 21, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (22, 1, 22, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (23, 1, 23, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (24, 1, 24, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (25, 1, 25, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (26, 1, 26, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (27, 1, 27, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (28, 1, 28, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (29, 1, 29, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (30, 1, 30, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (31, 1, 31, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (32, 1, 32, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (33, 1, 33, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (34, 1, 34, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (35, 1, 35, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (36, 1, 36, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (37, 1, 37, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (38, 1, 38, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (39, 1, 39, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (40, 1, 40, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (41, 1, 41, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (42, 1, 42, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (43, 1, 43, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (44, 1, 44, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (45, 1, 45, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (46, 1, 46, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (47, 1, 47, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (48, 1, 48, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (49, 1, 49, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (50, 1, 50, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (51, 1, 51, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (52, 1, 52, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (53, 1, 53, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (54, 1, 54, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (55, 1, 55, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (56, 1, 56, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (57, 1, 57, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (58, 1, 58, 1, 1, 1, 1)
INSERT [dbo].[VaiTro_ChucNang] ([id_VT_CN], [id_VaiTro], [id_ChucNang], [Them], [Xoa], [Sua], [CapNhat]) VALUES (59, 1, 59, 1, 1, 1, 1)
ALTER TABLE [dbo].[NhatKy] ADD  CONSTRAINT [DF_NhatKy_Date]  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [dbo].[BanHang]  WITH CHECK ADD  CONSTRAINT [FK_BanHang_KhachHang] FOREIGN KEY([id_khachhang])
REFERENCES [dbo].[KhachHang] ([Id_KH])
GO
ALTER TABLE [dbo].[BanHang] CHECK CONSTRAINT [FK_BanHang_KhachHang]
GO
ALTER TABLE [dbo].[BanHang]  WITH CHECK ADD  CONSTRAINT [FK_BanHang_Kho] FOREIGN KEY([Id_kho])
REFERENCES [dbo].[Kho] ([Id_Kho])
GO
ALTER TABLE [dbo].[BanHang] CHECK CONSTRAINT [FK_BanHang_Kho]
GO
ALTER TABLE [dbo].[ChiTien]  WITH CHECK ADD  CONSTRAINT [FK_ChiTien_KhuVuc] FOREIGN KEY([id_KhuVuc])
REFERENCES [dbo].[KhuVuc] ([Id_KhuVuc])
GO
ALTER TABLE [dbo].[ChiTien] CHECK CONSTRAINT [FK_ChiTien_KhuVuc]
GO
ALTER TABLE [dbo].[ChiTien]  WITH CHECK ADD  CONSTRAINT [FK_ChiTien_NhaCungCap] FOREIGN KEY([id_NCC])
REFERENCES [dbo].[NhaCungCap] ([id_NCC])
GO
ALTER TABLE [dbo].[ChiTien] CHECK CONSTRAINT [FK_ChiTien_NhaCungCap]
GO
ALTER TABLE [dbo].[ChiTietBanHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietBanHang_BanHang] FOREIGN KEY([id_ChungTuXuat])
REFERENCES [dbo].[BanHang] ([id_ChungTuban])
GO
ALTER TABLE [dbo].[ChiTietBanHang] CHECK CONSTRAINT [FK_ChiTietBanHang_BanHang]
GO
ALTER TABLE [dbo].[ChiTietBanHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietBanHang_DonViTinh] FOREIGN KEY([id_donvi])
REFERENCES [dbo].[DonViTinh] ([Id_DV])
GO
ALTER TABLE [dbo].[ChiTietBanHang] CHECK CONSTRAINT [FK_ChiTietBanHang_DonViTinh]
GO
ALTER TABLE [dbo].[ChiTietBanHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietBanHang_HangHoa] FOREIGN KEY([Id_HangHoa])
REFERENCES [dbo].[HangHoa] ([id_hanghoa])
GO
ALTER TABLE [dbo].[ChiTietBanHang] CHECK CONSTRAINT [FK_ChiTietBanHang_HangHoa]
GO
ALTER TABLE [dbo].[ChiTietNhapHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietNhapHang_DonViTinh] FOREIGN KEY([id_DonVi])
REFERENCES [dbo].[DonViTinh] ([Id_DV])
GO
ALTER TABLE [dbo].[ChiTietNhapHang] CHECK CONSTRAINT [FK_ChiTietNhapHang_DonViTinh]
GO
ALTER TABLE [dbo].[ChiTietNhapHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietNhapHang_HangHoa] FOREIGN KEY([id_HangHoa])
REFERENCES [dbo].[HangHoa] ([id_hanghoa])
GO
ALTER TABLE [dbo].[ChiTietNhapHang] CHECK CONSTRAINT [FK_ChiTietNhapHang_HangHoa]
GO
ALTER TABLE [dbo].[ChiTietNhapHang]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietNhapHang_NhapHang] FOREIGN KEY([id_ChungTuNhap])
REFERENCES [dbo].[NhapHang] ([id_ChungTuNhap])
GO
ALTER TABLE [dbo].[ChiTietNhapHang] CHECK CONSTRAINT [FK_ChiTietNhapHang_NhapHang]
GO
ALTER TABLE [dbo].[ChuyenKho]  WITH CHECK ADD  CONSTRAINT [FK_ChuyenKho_Kho] FOREIGN KEY([id_Kho])
REFERENCES [dbo].[Kho] ([Id_Kho])
GO
ALTER TABLE [dbo].[ChuyenKho] CHECK CONSTRAINT [FK_ChuyenKho_Kho]
GO
ALTER TABLE [dbo].[HangHoa]  WITH CHECK ADD  CONSTRAINT [FK_HangHoa_DonViTinh] FOREIGN KEY([id_DonVi])
REFERENCES [dbo].[DonViTinh] ([Id_DV])
GO
ALTER TABLE [dbo].[HangHoa] CHECK CONSTRAINT [FK_HangHoa_DonViTinh]
GO
ALTER TABLE [dbo].[HangHoa]  WITH CHECK ADD  CONSTRAINT [FK_HangHoa_Kho] FOREIGN KEY([id_kho])
REFERENCES [dbo].[Kho] ([Id_Kho])
GO
ALTER TABLE [dbo].[HangHoa] CHECK CONSTRAINT [FK_HangHoa_Kho]
GO
ALTER TABLE [dbo].[HangHoa]  WITH CHECK ADD  CONSTRAINT [FK_HangHoa_NhomHang] FOREIGN KEY([id_nhomhang])
REFERENCES [dbo].[NhomHang] ([Id_NhomHang])
GO
ALTER TABLE [dbo].[HangHoa] CHECK CONSTRAINT [FK_HangHoa_NhomHang]
GO
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [FK_KhachHang_KhuVuc] FOREIGN KEY([Id_KhuVuc])
REFERENCES [dbo].[KhuVuc] ([Id_KhuVuc])
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [FK_KhachHang_KhuVuc]
GO
ALTER TABLE [dbo].[MaVach]  WITH CHECK ADD  CONSTRAINT [FK_MaVach_HangHoa] FOREIGN KEY([id_Hang])
REFERENCES [dbo].[HangHoa] ([id_hanghoa])
GO
ALTER TABLE [dbo].[MaVach] CHECK CONSTRAINT [FK_MaVach_HangHoa]
GO
ALTER TABLE [dbo].[NguoiDung]  WITH CHECK ADD  CONSTRAINT [FK_NguoiDung_VaiTro] FOREIGN KEY([id_VaiTro])
REFERENCES [dbo].[VaiTro] ([idVaiTro])
GO
ALTER TABLE [dbo].[NguoiDung] CHECK CONSTRAINT [FK_NguoiDung_VaiTro]
GO
ALTER TABLE [dbo].[NhaCungCap]  WITH CHECK ADD  CONSTRAINT [FK_NhaCungCap_KhuVuc] FOREIGN KEY([id_KV])
REFERENCES [dbo].[KhuVuc] ([Id_KhuVuc])
GO
ALTER TABLE [dbo].[NhaCungCap] CHECK CONSTRAINT [FK_NhaCungCap_KhuVuc]
GO
ALTER TABLE [dbo].[NhapHang]  WITH CHECK ADD  CONSTRAINT [FK_NhapHang_Kho] FOREIGN KEY([id_Kho])
REFERENCES [dbo].[Kho] ([Id_Kho])
GO
ALTER TABLE [dbo].[NhapHang] CHECK CONSTRAINT [FK_NhapHang_Kho]
GO
ALTER TABLE [dbo].[NhapHang]  WITH CHECK ADD  CONSTRAINT [FK_NhapHang_NhaCungCap] FOREIGN KEY([id_NhaCungCap])
REFERENCES [dbo].[NhaCungCap] ([id_NCC])
GO
ALTER TABLE [dbo].[NhapHang] CHECK CONSTRAINT [FK_NhapHang_NhaCungCap]
GO
ALTER TABLE [dbo].[NhatKy]  WITH CHECK ADD  CONSTRAINT [FK_NhatKy_NguoiDung] FOREIGN KEY([id_NguoiDung])
REFERENCES [dbo].[NguoiDung] ([id_NguoiDung])
GO
ALTER TABLE [dbo].[NhatKy] CHECK CONSTRAINT [FK_NhatKy_NguoiDung]
GO
ALTER TABLE [dbo].[ThuTien]  WITH CHECK ADD  CONSTRAINT [FK_ThuTien_KhachHang] FOREIGN KEY([id_KhachHang])
REFERENCES [dbo].[KhachHang] ([Id_KH])
GO
ALTER TABLE [dbo].[ThuTien] CHECK CONSTRAINT [FK_ThuTien_KhachHang]
GO
ALTER TABLE [dbo].[ThuTien]  WITH CHECK ADD  CONSTRAINT [FK_ThuTien_KhuVuc] FOREIGN KEY([id_KhuVuc])
REFERENCES [dbo].[KhuVuc] ([Id_KhuVuc])
GO
ALTER TABLE [dbo].[ThuTien] CHECK CONSTRAINT [FK_ThuTien_KhuVuc]
GO
ALTER TABLE [dbo].[TonKho]  WITH CHECK ADD  CONSTRAINT [FK_TonKho_DonViTinh] FOREIGN KEY([id_donvi])
REFERENCES [dbo].[DonViTinh] ([Id_DV])
GO
ALTER TABLE [dbo].[TonKho] CHECK CONSTRAINT [FK_TonKho_DonViTinh]
GO
ALTER TABLE [dbo].[TonKho]  WITH CHECK ADD  CONSTRAINT [FK_TonKho_HangHoa] FOREIGN KEY([id_MaHang])
REFERENCES [dbo].[HangHoa] ([id_hanghoa])
GO
ALTER TABLE [dbo].[TonKho] CHECK CONSTRAINT [FK_TonKho_HangHoa]
GO
ALTER TABLE [dbo].[TonKho]  WITH CHECK ADD  CONSTRAINT [FK_TonKho_Kho] FOREIGN KEY([id_Kho])
REFERENCES [dbo].[Kho] ([Id_Kho])
GO
ALTER TABLE [dbo].[TonKho] CHECK CONSTRAINT [FK_TonKho_Kho]
GO
ALTER TABLE [dbo].[TonKho]  WITH CHECK ADD  CONSTRAINT [FK_TonKho_NhomHang] FOREIGN KEY([id_NhomHang])
REFERENCES [dbo].[NhomHang] ([Id_NhomHang])
GO
ALTER TABLE [dbo].[TonKho] CHECK CONSTRAINT [FK_TonKho_NhomHang]
GO
ALTER TABLE [dbo].[VaiTro_ChucNang]  WITH CHECK ADD  CONSTRAINT [FK_VaiTro_ChucNang_ChucNang] FOREIGN KEY([id_ChucNang])
REFERENCES [dbo].[ChucNang] ([id_ChucNang])
GO
ALTER TABLE [dbo].[VaiTro_ChucNang] CHECK CONSTRAINT [FK_VaiTro_ChucNang_ChucNang]
GO
ALTER TABLE [dbo].[VaiTro_ChucNang]  WITH CHECK ADD  CONSTRAINT [FK_VaiTro_ChucNang_VaiTro] FOREIGN KEY([id_VaiTro])
REFERENCES [dbo].[VaiTro] ([idVaiTro])
GO
ALTER TABLE [dbo].[VaiTro_ChucNang] CHECK CONSTRAINT [FK_VaiTro_ChucNang_VaiTro]
GO
USE [master]
GO
ALTER DATABASE [DACK_ware1] SET  READ_WRITE 
GO
